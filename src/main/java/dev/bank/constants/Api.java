package dev.bank.constants;

public class Api {
    public static final String ID = "/{id}";
    public static final String RECHARGE = "/recharge";
    public static final String WITHDRAW = "/withdraw";
    public static final String TRANSFER = "/transfer";
    public static final String ACCOUNTS = "/api/accounts";
    public static final String ACCOUNTS_RECHARGE = "/api/accounts/recharge";
    public static final String ACCOUNTS_WITHDRAW = "/api/accounts/withdraw";
    public static final String ACCOUNTS_TRANSFER = "/api/accounts/transfer";

    public static final String CUSTOMERS = "/api/customers";

    public static final String CURRENCIES = "/api/currencies";
    public static final String CURRENCIES_EXCHANGE = "/api/currencies/exchange";
    public static final String CURRENCY_MONO = "https://api.monobank.ua/bank/currency";


    public static final String EMPLOYERS = "/api/employers";
    public static final String EMPLOYERS_ID_CUSTOMER_ID = "/{employerId}/customer/{customerId}";
}

