package dev.bank.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import dev.bank.dto.customer.CustomerWithoutAccounts;
import dev.bank.entities.Account;

import java.io.IOException;

public class AccountSerializer extends StdSerializer<Account> {
    public AccountSerializer() {
        this(null);
    }

    public AccountSerializer(Class<Account> t) {
        super(t);
    }

    @Override
    public void serialize(
            Account value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();
        jgen.writeNumberField("id", value.getId());
        jgen.writeStringField("number", value.getNumber());
        jgen.writeStringField("currency", value.getCurrency().toString());
        jgen.writeNumberField("balance", value.getBalance());
        jgen.writeObjectField("customer", CustomerWithoutAccounts.of(value.getCustomer()));
        jgen.writeEndObject();
    }
}
