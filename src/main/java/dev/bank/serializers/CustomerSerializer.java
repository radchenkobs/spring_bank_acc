package dev.bank.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import dev.bank.dto.account.AccountWithoutCustomer;
import dev.bank.entities.Account;
import dev.bank.entities.Customer;

import java.io.IOException;

public class CustomerSerializer extends StdSerializer<Customer> {
    public CustomerSerializer() {
        this(null);
    }

    public CustomerSerializer(Class<Customer> t) {
        super(t);
    }

    @Override
    public void serialize(
            Customer value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();
        jgen.writeNumberField("id", value.getId());
        jgen.writeStringField("name", value.getName());
        jgen.writeStringField("email", value.getEmail());
        jgen.writeNumberField("age", value.getAge());
        jgen.writeArrayFieldStart("accounts");
        for (Account account : value.getAccounts()) {
            jgen.writeObject(AccountWithoutCustomer.of(account));
        }
        jgen.writeEndArray();
        jgen.writeEndObject();
    }
}
