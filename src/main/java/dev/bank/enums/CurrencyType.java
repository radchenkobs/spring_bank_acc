package dev.bank.enums;

import dev.bank.exceptions.NotFoundException;
import lombok.Getter;
import lombok.ToString;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.util.Arrays;
import java.util.Optional;

@Getter
//@ToString(of = {"symbol", "value", "code"})
@ToString(of = {"value"})
public enum CurrencyType {
    USD("$"), EUR("€"), UAH("₴"), CHF("Fr"), GBP("£");

    public final static int MAX_CURRENCY_ORDINAL = 4;

    private final CurrencyUnit unit;
    private final String symbol;
    private final String value;
    private final Integer code;

    CurrencyType(String symbol) {
        this.unit = Monetary.getCurrency(this.name());
        this.symbol = symbol;
        this.value = unit.getCurrencyCode();
        this.code = unit.getNumericCode();
    }

    public boolean equals(CurrencyType that) {
        return this.name().equals(that.name());
    }

    public static CurrencyType of(String name) throws NotFoundException {
        Optional<CurrencyType> curr = Arrays.stream(CurrencyType.values()).filter(currency -> currency.name().equals(name)).findFirst();
        if (curr.isEmpty()) {
            throw new NotFoundException(String.format("Currency witch name: %s not found", name));
        }
        return curr.get();
    }

    public static CurrencyType of(int ordinal) throws NotFoundException {
        CurrencyType[] values = CurrencyType.values();
        if (ordinal < 0 || ordinal >= values.length) {
            throw new NotFoundException(String.format("Currency witch ordinal: %s not found", ordinal));
        }
        return CurrencyType.values()[ordinal];
    }
}
