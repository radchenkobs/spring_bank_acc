package dev.bank.controllers;

import dev.bank.constants.Api;
import dev.bank.dto.ResponseCurrency;
import dev.bank.enums.CurrencyType;
import dev.bank.responses.ResponseSuccess;
import dev.bank.services.CustomerService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@Log4j2
@RequestMapping(Api.CURRENCIES)
@AllArgsConstructor
public class CurrencyController {
    private final CustomerService customerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<List<ResponseCurrency>> getAll() {
        List<ResponseCurrency> res = Arrays.stream(CurrencyType.values())
                .map(ResponseCurrency::of)
                .toList();
        return ResponseSuccess.of(res);
    }
}
