package dev.bank.controllers;

import dev.bank.constants.Api;
import dev.bank.dto.customer.RequestCustomerCreate;
import dev.bank.dto.customer.RequestCustomerUpdate;
import dev.bank.dto.customer.ResponseCustomer;
import dev.bank.responses.ResponseSuccess;
import dev.bank.services.CustomerService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Log4j2
@RequestMapping(Api.CUSTOMERS)
@AllArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<List<ResponseCustomer>> getAll() {
        return ResponseSuccess.of(customerService.getAll());
    }

    @GetMapping(Api.ID)
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<ResponseCustomer> getById(@PathVariable("id") Long id) {
        return ResponseSuccess.of(customerService.getById(id));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseSuccess<ResponseCustomer> create(@RequestBody @Valid RequestCustomerCreate data) {
        return ResponseSuccess.of(customerService.create(data));
    }

    @DeleteMapping(Api.ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        customerService.delete(id);
    }

    @PatchMapping(path = {Api.ID}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<ResponseCustomer> patch(@PathVariable("id") Long id, @RequestBody @Valid RequestCustomerUpdate data) {
        return ResponseSuccess.of(customerService.update(id, data));
    }
}
