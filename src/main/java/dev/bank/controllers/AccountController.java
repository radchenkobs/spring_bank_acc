package dev.bank.controllers;

import dev.bank.constants.Api;
import dev.bank.dto.account.*;
import dev.bank.responses.ResponseSuccess;
import dev.bank.services.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Api.ACCOUNTS)
@AllArgsConstructor
public class AccountController {
    private final AccountService accountService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<List<ResponseAccount>> getAll() {
        return ResponseSuccess.of(accountService.getAll());
    }

    @GetMapping(Api.ID)
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<ResponseAccount> accountById(@PathVariable("id") Long id) {
        return ResponseSuccess.of(accountService.getById(id));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseAccount create(@RequestBody RequestAccountCreate data) {
        return accountService.create(data);
    }

    @DeleteMapping(Api.ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable("id") Long id) {
        accountService.deleteById(id);
    }

    @PostMapping(value = Api.RECHARGE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<ResponseAccount> recharge(@RequestBody RequestAccountAmount data) {
        ResponseAccount account = accountService.recharge(data);
        return ResponseSuccess.of(account);
    }

    @PostMapping(value = Api.WITHDRAW, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<ResponseAccount> withdraw(@RequestBody RequestAccountAmount data) {
        ResponseAccount account = accountService.withdraw(data);
        return ResponseSuccess.of(account);
    }

    @PostMapping(value = Api.TRANSFER, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<ResponseAccountTransfer> transfer(@RequestBody RequestAccountTransfer data) {
        return ResponseSuccess.of(accountService.transfer(data));
    }
}
