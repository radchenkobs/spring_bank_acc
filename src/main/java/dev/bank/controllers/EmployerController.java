package dev.bank.controllers;

import dev.bank.constants.Api;
import dev.bank.dto.employer.RequestEmployerCreate;
import dev.bank.dto.employer.ResponseEmployer;
import dev.bank.responses.ResponseSuccess;
import dev.bank.services.EmployerService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Api.EMPLOYERS)
@AllArgsConstructor
public class EmployerController {
    private final EmployerService employerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<List<ResponseEmployer>> getAll() {
        return ResponseSuccess.of(employerService.getAll());
    }

    @GetMapping(Api.ID)
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<ResponseEmployer> getById(@PathVariable("id") Long id) {
        return ResponseSuccess.of(employerService.getById(id));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseSuccess<ResponseEmployer> create(@RequestBody @Valid RequestEmployerCreate data) {
        return ResponseSuccess.of(employerService.create(data));
    }

    @DeleteMapping(Api.EMPLOYERS_ID_CUSTOMER_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable("employerId") Long employerId, @PathVariable("customerId") Long customerId) {
        employerService.removeCustomer(employerId, customerId);
    }

    @PutMapping(Api.EMPLOYERS_ID_CUSTOMER_ID)
    @ResponseStatus(HttpStatus.OK)
    public ResponseSuccess<ResponseEmployer> addCustomer(@PathVariable("employerId") Long employerId, @PathVariable("customerId") Long customerId) {
        ResponseEmployer employer = employerService.addCustomer(employerId, customerId);
        return ResponseSuccess.of(employer);
    }


}
