package dev.bank.controllers;

import dev.bank.exceptions.NotFoundException;
import dev.bank.responses.ResponseError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
public class ExceptionRestControllerAdvice {
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ResponseError> handleValidationErrors(MethodArgumentNotValidException x) {
        List<String> errors = x
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(FieldError::getDefaultMessage)
                .toList();
        errors.forEach(System.out::println);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ResponseError.of(x.getBody().getDetail(), errors));
    }


    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ResponseError> notFound(NotFoundException x) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(ResponseError.of(x.getMessage()));
    }

}
