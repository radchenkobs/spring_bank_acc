package dev.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankApplication {
    // TODO: GraphQl ??
    // TODO: hw2?
    // TODO: AppException // @ControllerAdvice
    // TODO: Entity
    // TODO: hibernate app yml
    // TODO: datasource
    // TODO: repo/models/dto
    // TODO: refactor services
    // TODO: logs

    public static void main(String[] args) {
        SpringApplication.run(BankApplication.class, args);
    }

}
