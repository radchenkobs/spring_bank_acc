package dev.bank.dto.employer;

import dev.bank.dto.customer.CustomerWithoutAccounts;
import dev.bank.entities.Employer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class ResponseEmployer {
    private Long id;
    private String name;
    private String address;
    private final List<CustomerWithoutAccounts> customers;

    public static ResponseEmployer of(Employer data) {
        return ResponseEmployer
                .builder()
                .id(data.getId())
                .name(data.getName())
                .address(data.getAddress())
                .customers(data.getCustomers().stream().map(CustomerWithoutAccounts::of).toList())
                .build();
    }

}
