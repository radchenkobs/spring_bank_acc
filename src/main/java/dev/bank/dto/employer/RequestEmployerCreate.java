package dev.bank.dto.employer;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class RequestEmployerCreate {
    @NotBlank(message = "Invalid Name: Empty name")
    @NotNull(message = "Invalid Name: Name is NULL")
    @Size(min = 2, max = 60, message = "Invalid Name: Must be of 2 - 60 characters")
    private String name;
    @NotBlank(message = "Invalid Address: Empty Address")
    @NotNull(message = "Invalid Address: Address is NULL")
    @Size(min = 2, max = 60, message = "Invalid Address: Must be of 2 - 60 characters")
    private String address;
}
