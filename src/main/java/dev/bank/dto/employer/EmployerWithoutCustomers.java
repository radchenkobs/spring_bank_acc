package dev.bank.dto.employer;

import dev.bank.entities.Employer;

public record EmployerWithoutCustomers(Long id, String name, String address) {

    public static EmployerWithoutCustomers of(Employer employer) {
        return new EmployerWithoutCustomers(
                employer.getId(),
                employer.getName(),
                employer.getAddress()
        );
    }
}
