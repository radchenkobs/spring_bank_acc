package dev.bank.dto.account;

import dev.bank.enums.CurrencyType;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequestAccountCreate {
    @NotNull(message = "Invalid customer id [customerId]: Id is NULL")
    @Min(value = 0, message = "Invalid customer id: Less then zero")
    private Long customerId;

    @Min(value = -1, message = "Invalid currency id [currencyId]: Less then zero")
    @Max(value = CurrencyType.MAX_CURRENCY_ORDINAL, message = "Invalid currency id [currencyId]: Ordinal more then %d " + CurrencyType.MAX_CURRENCY_ORDINAL)
    private Integer currencyId;
}
