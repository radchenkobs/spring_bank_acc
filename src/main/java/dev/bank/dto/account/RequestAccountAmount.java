package dev.bank.dto.account;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequestAccountAmount {
    @NotBlank(message = "Invalid Number: Empty number")
    @NotNull(message = "Invalid Number: Number is NULL")
    @Size(min = 16, max = 16, message = "Invalid Number: Must be 16 characters")
    private String number;
    @NotNull(message = "Invalid amount value: amount is NULL")
    @Min(value = 0, message = "Invalid amount value: Less then zero")
    private Double amount;

}
