package dev.bank.dto.account;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequestAccountTransfer {
    @NotBlank(message = "Invalid Number FROM: Empty Number FROM")
    @NotNull(message = "Invalid Number FROM: Number FROM is NULL")
    @Size(min = 16, max = 16, message = "Invalid Number FROM: Must be 16 characters")
    private String from;
    @NotBlank(message = "Invalid Number TO: Empty Number TO")
    @NotNull(message = "Invalid Number TO: Number TO is NULL")
    @Size(min = 16, max = 16, message = "Invalid Number TO: Must be 16 characters")
    private String to;
    @NotNull(message = "Invalid amount value: amount is NULL")
    @Min(value = 0, message = "Invalid amount value: Less then zero")
    private Double amount;

}
