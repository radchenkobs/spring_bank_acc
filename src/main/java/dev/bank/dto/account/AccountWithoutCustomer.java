package dev.bank.dto.account;

import dev.bank.entities.Account;
import dev.bank.enums.CurrencyType;

public record AccountWithoutCustomer(Long id, String number, CurrencyType currency, Double balance) {

    public static AccountWithoutCustomer of(Account account) {
        return new AccountWithoutCustomer(
                account.getId(),
                account.getNumber(),
                account.getCurrency(),
                account.getBalance()
        );
    }
}
