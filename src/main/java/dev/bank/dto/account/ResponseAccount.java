package dev.bank.dto.account;

import dev.bank.dto.customer.CustomerWithoutAccounts;
import dev.bank.entities.Account;
import dev.bank.enums.CurrencyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ResponseAccount {
    private Long id;
    private String number;
    private CurrencyType currency;
    private Double balance;
    private CustomerWithoutAccounts customer;

    public static ResponseAccount of(Account data) {
        return ResponseAccount
                .builder()
                .id(data.getId())
                .number(data.getNumber())
                .currency(data.getCurrency())
                .balance(data.getBalance())
                .customer(CustomerWithoutAccounts.of(data.getCustomer()))
                .build();
    }

}
