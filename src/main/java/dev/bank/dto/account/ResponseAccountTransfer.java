package dev.bank.dto.account;

import dev.bank.entities.Account;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ResponseAccountTransfer {
    private ResponseAccount from;
    private ResponseAccount to;

    private Double amount;
    private Double converted;

    public static ResponseAccountTransfer of(
            Account from,
            Account to,
            Double amount,
            Double converted
    ) {
        return ResponseAccountTransfer
                .builder()
                .from(ResponseAccount.of(from))
                .to(ResponseAccount.of(to))
                .amount(amount)
                .converted(converted)
                .build();
    }

}
