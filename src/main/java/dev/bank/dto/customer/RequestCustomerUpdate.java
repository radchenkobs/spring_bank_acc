package dev.bank.dto.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestCustomerUpdate {
    @Size(min = 2, max = 30, message = "Invalid Name: Must be of 2 - 30 characters")
    private String name;
    @Email(message = "Invalid email")
    private String email;
    @Min(value = 18, message = "Invalid Age: Less than 18")
    @Max(value = 100, message = "Invalid Age: Exceeds 100 years")
    private Integer age;

}
