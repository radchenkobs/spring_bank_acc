package dev.bank.dto.customer;

import dev.bank.dto.account.AccountWithoutCustomer;
import dev.bank.dto.employer.EmployerWithoutCustomers;
import dev.bank.entities.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class ResponseCustomer {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<AccountWithoutCustomer> accounts;
    private List<EmployerWithoutCustomers> employers;

    public static ResponseCustomer of(Customer data) {
        if (data.getAccounts() == null) {
            data.setAccounts(new ArrayList<>());
        }
        if (data.getEmployers() == null) {
            data.setEmployers(new ArrayList<>());
        }
        return ResponseCustomer
                .builder()
                .id(data.getId())
                .name(data.getName())
                .email(data.getEmail())
                .age(data.getAge())
                .accounts(data.getAccounts().stream()
                        .map(AccountWithoutCustomer::of)
                        .toList())
                .employers(data.getEmployers().stream()
                        .map(EmployerWithoutCustomers::of)
                        .toList())
                .build();
    }

}
