package dev.bank.dto.customer;

import jakarta.validation.constraints.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RequestCustomerCreate {
    @NotBlank(message = "Invalid Name: Empty name")
    @NotNull(message = "Invalid Name: Name is NULL")
    @Size(min = 2, max = 30, message = "Invalid Name: Must be of 2 - 30 characters")
    private String name;
    @Email(message = "Invalid email")
    private String email;
    @Min(value = 18, message = "Invalid Age: Less than 18")
    @Max(value = 100, message = "Invalid Age: Exceeds 100 years")
    private Integer age;

    private List<String> currencies = new ArrayList<>();

    private List<Long> employers = new ArrayList<>();
}
