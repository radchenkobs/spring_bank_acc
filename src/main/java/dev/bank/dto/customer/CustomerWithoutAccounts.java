package dev.bank.dto.customer;

import dev.bank.entities.Customer;

public record CustomerWithoutAccounts(Long id, String name, String email) {

    public static CustomerWithoutAccounts of(Customer customer) {
        return new CustomerWithoutAccounts(
                customer.getId(),
                customer.getName(),
                customer.getEmail()
        );
    }
}
