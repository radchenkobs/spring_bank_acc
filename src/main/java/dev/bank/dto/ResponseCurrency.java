package dev.bank.dto;

import dev.bank.enums.CurrencyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ResponseCurrency {
    private Integer id;
    private String value;
    private String symbol;
    private Integer code;

    public static ResponseCurrency of(CurrencyType currency) {
        System.out.println(currency);
        return ResponseCurrency
                .builder()
                .id(currency.ordinal())
                .value(currency.getValue())
                .symbol(currency.getSymbol())
                .code(currency.getCode())
                .build();
    }

}
