package dev.bank.services;

import dev.bank.dto.customer.RequestCustomerCreate;
import dev.bank.dto.customer.RequestCustomerUpdate;
import dev.bank.dto.customer.ResponseCustomer;
import dev.bank.entities.Customer;
import dev.bank.exceptions.AlreadyExistException;
import dev.bank.exceptions.NotFoundException;
import dev.bank.repo.CustomerRepo;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private final CustomerRepo customerRepo;
    private final AccountService accountService;
    private final EmployerService employerService;

    public CustomerService(@Lazy AccountService accountService, @Lazy EmployerService employerService, CustomerRepo customerRepo) {
        this.accountService = accountService;
        this.employerService = employerService;
        this.customerRepo = customerRepo;
    }

    public void notFoundEx(Long id) {
        throw new NotFoundException(String.format("Customer by ID: %d not found", id));
    }

    public void alreadyExist(String email) throws AlreadyExistException {
        throw new AlreadyExistException(String.format("Customer with EMAIL: %s already exist", email));
    }

    public boolean isExist(Long id) {
        return customerRepo.existsById(id);
    }

    public boolean isExist(String email) {
        return customerRepo.existsByEmail(email);
    }

    public ResponseCustomer save(Customer customer) {
        Customer savedCustomer = customerRepo.save(customer);
        return ResponseCustomer.of(savedCustomer);
    }

    public List<ResponseCustomer> getAll() {
        return customerRepo
                .findAll()
                .stream()
                .map(ResponseCustomer::of)
                .toList();
    }

    public ResponseCustomer create(RequestCustomerCreate data) throws AlreadyExistException, NotFoundException {
        if (isExist(data.getEmail())) {
            alreadyExist(data.getEmail());
        }
        Customer customer = customerRepo.save(Customer.of(data));
        if (data.getCurrencies() != null) {
            accountService.create(data.getCurrencies(), customer);
        }
        if (data.getEmployers() != null) {
            employerService.add(data.getEmployers(), customer);
        }
        return getById(customer.getId());
    }

    public Customer findById(Long id) throws NotFoundException {
        Optional<Customer> customerOptional = customerRepo.findById(id);
        if (customerOptional.isEmpty()) {
            notFoundEx(id);
        }
        return customerOptional.get();
    }

    public ResponseCustomer getById(Long id) throws NotFoundException {
        Customer customer = findById(id);
        return ResponseCustomer.of(customer);
    }

    public void delete(Long id) throws NotFoundException {
        if (!isExist(id)) {
            notFoundEx(id);
        }
        customerRepo.deleteById(id);
    }

    public ResponseCustomer update(Long id, RequestCustomerUpdate data) throws NotFoundException {
        Optional<Customer> customerById = customerRepo.findById(id);
        if (customerById.isEmpty()) {
            notFoundEx(id);
        }
        return save(Customer.of(customerById.get(), data));
    }
}