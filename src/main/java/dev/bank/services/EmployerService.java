package dev.bank.services;

import dev.bank.dto.employer.RequestEmployerCreate;
import dev.bank.dto.employer.ResponseEmployer;
import dev.bank.entities.Customer;
import dev.bank.entities.Employer;
import dev.bank.exceptions.AlreadyExistException;
import dev.bank.exceptions.InternalServerException;
import dev.bank.exceptions.NotFoundException;
import dev.bank.repo.EmployerRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class EmployerService {
    private final EmployerRepo employerRepo;
    private final CustomerService customerService;

    public void alreadyExist(String name) throws AlreadyExistException {
        throw new AlreadyExistException(String.format("Employer with name: %s already exist", name));
    }

    public void notFound(Long id) throws NotFoundException {
        throw new AlreadyExistException(String.format("Employer with ID: %d not found", id));
    }

    public boolean isExist(String name) {
        return employerRepo.existsByName(name);
    }

    public boolean isExist(Long id) {
        return employerRepo.existsById(id);
    }

    public List<ResponseEmployer> getAll() {
        return employerRepo
                .findAll()
                .stream()
                .map(ResponseEmployer::of)
                .toList();
    }

    public ResponseEmployer save(Employer employer) {
        Employer savedEmployer = employerRepo.save(employer);
        return ResponseEmployer.of(savedEmployer);
    }

    public Employer findById(Long id) throws NotFoundException {
        Optional<Employer> employerOptional = employerRepo.findById(id);
        if (employerOptional.isEmpty()) {
            throw new NotFoundException(String.format("Employer with ID:%d not found", id));
        }
        return employerOptional.get();
    }

    public ResponseEmployer getById(Long id) throws NotFoundException {
        return ResponseEmployer.of(findById(id));
    }

    public ResponseEmployer create(RequestEmployerCreate data) throws AlreadyExistException {
        if (isExist(data.getName())) {
            alreadyExist(data.getName());
        }
        Employer employer = employerRepo.save(Employer.of(data));
        return ResponseEmployer.of(employer);
    }

    public Employer add(Long employerId, Customer customer) throws NotFoundException {
        Employer employer = findById(employerId);
        employer.getCustomers().add(customer);
        return employerRepo.save(employer);
    }

    public void add(List<Long> employerIds, final Customer customer) {
        employerIds
                .stream()
                .filter(this::isExist)
                .forEach(id -> add(id, customer));
    }

    public ResponseEmployer removeCustomer(Long employerId, Long customerId) throws NotFoundException, InternalServerException {
        Employer employer = findById(employerId);
        Customer customer = customerService.findById(customerId);
        boolean status = employer.getCustomers().remove(customer);
        if (!status) {
            throw new InternalServerException(String.format(
                    "Error remove customer ID: %d from employer ID: %d",
                    customerId,
                    employerId
            ));
        }
        return save(employer);
    }

    public ResponseEmployer addCustomer(Long employerId, Long customerId) throws NotFoundException {
        Employer employer = findById(employerId);
        Customer customer = customerService.findById(customerId);
        employer.getCustomers().add(customer);
        return save(employer);
    }
}