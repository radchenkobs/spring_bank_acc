package dev.bank.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import dev.bank.dto.account.*;
import dev.bank.entities.Account;
import dev.bank.entities.CurrencyExchange;
import dev.bank.entities.Customer;
import dev.bank.enums.CurrencyType;
import dev.bank.exceptions.ForbiddenException;
import dev.bank.exceptions.NotFoundException;
import dev.bank.repo.AccountRepo;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.hibernate.Transaction;
import org.hibernate.engine.spi.SessionImplementor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class AccountService {
    public final CurrencyService currencyService;
    public final CustomerService customerService;
    public final AccountRepo accountRepo;
    private final EntityManagerFactory emf;

    public void notFoundEx(Long id) {
        throw new NotFoundException(String.format("Account by ID: %d not found", id));
    }

    public void notFoundEx(String number) {
        throw new NotFoundException(String.format("AccountProfile with number: %s not found", number));
    }

    public boolean isExist(Long id) {
        return accountRepo.existsById(id);
    }

    public List<ResponseAccount> getAll() {
        return accountRepo
                .findAll()
                .stream()
                .map(ResponseAccount::of)
                .toList();
    }

    public ResponseAccount getById(Long id) throws NotFoundException {
        Optional<Account> account = accountRepo.findById(id);
        if (account.isEmpty()) {
            notFoundEx(id);
        }
        return ResponseAccount.of(account.get());
    }

    public Account findByNumber(String number) throws NotFoundException {
        Optional<Account> acc = accountRepo.findByNumber(number);
        if (acc.isEmpty()) {
            notFoundEx(number);
        }
        return acc.get();
    }

    public ResponseAccount getByNumber(String number) throws NotFoundException {
        return ResponseAccount.of(findByNumber(number));
    }

    public ResponseAccount save(Account account) {
        return ResponseAccount.of(accountRepo.save(account));
    }

    public ResponseAccount create(RequestAccountCreate data) throws NotFoundException {
        Customer customer = customerService.findById(data.getCustomerId());
        Account acc = accountRepo.save(Account.of(data, customer));
        customer.addAccount(acc);
        return ResponseAccount.of(acc);
    }

    public ResponseAccount create(String currency, Customer customer) throws NotFoundException {
        RequestAccountCreate data = new RequestAccountCreate(customer.getId(), CurrencyType.of(currency).ordinal());
        Account acc = accountRepo.save(Account.of(data, customer));
        customer.addAccount(acc);
        return ResponseAccount.of(acc);
    }

    public List<ResponseAccount> create(List<String> currencies, Customer customer) throws NotFoundException {
        return currencies
                .stream()
                .map(c -> create(c, customer))
                .toList();
    }

    public void deleteById(Long id) throws NotFoundException {
        if (!isExist(id)) {
            notFoundEx(id);
        }
        accountRepo.deleteById(id);
    }

    public ResponseAccount recharge(RequestAccountAmount data) throws NotFoundException {
        Account account = findByNumber(data.getNumber());
        account.recharge(data.getAmount());
        return save(account);
    }

    public ResponseAccount withdraw(RequestAccountAmount data) throws NotFoundException, ForbiddenException {
        Account account = findByNumber(data.getNumber());
        account.withdraw(data.getAmount());
        return save(account);
    }

    public Double convert(Account a, Account b, Double amount) throws RuntimeException {
        if (a.getCurrency().equals(b.getCurrency())) return amount;
        try {
            List<CurrencyExchange> currencies = currencyService.getCurrencyMono();
            Optional<CurrencyExchange> currencyOption = currencies
                    .stream()
                    .filter(c ->
                            (c.currencyCodeA().equals(a.getCurrency().getCode()) && c.currencyCodeB().equals(b.getCurrency().getCode()))
                                    || c.currencyCodeA().equals(b.getCurrency().getCode()) && c.currencyCodeB().equals(a.getCurrency().getCode())
                    )
                    .findFirst();
            if (currencyOption.isEmpty()) {
                throw new RuntimeException(String.format(
                        "Currency %s & %s not found",
                        a.getCurrency().getCode(),
                        b.getCurrency().getCode()));
            }

            CurrencyExchange currency = currencyOption.get();

            return a.getCurrency().getCode().equals(currency.currencyCodeA())
                    ? amount * currency.rateBuy()
                    : amount / currency.rateSell();

        } catch (JsonProcessingException ex) {
            throw new RuntimeException("Convert unknown error", ex);
        }

    }

    @Transactional
    public ResponseAccountTransfer transfer(RequestAccountTransfer data) throws NotFoundException, ForbiddenException {
        EntityManager em = emf.createEntityManager();
        SessionImplementor sessionImp = (SessionImplementor) em.getDelegate();
        Transaction transaction = sessionImp.getTransaction();

        try {
            transaction.begin();

            Account from = findByNumber(data.getFrom());
            Account to = findByNumber(data.getTo());

            Double convertAmount = convert(from, to, data.getAmount());


            from.withdraw(data.getAmount());
            to.recharge(convertAmount);

            transaction.commit();
            return ResponseAccountTransfer.of(from, to, data.getAmount(), convertAmount);
        } catch (ForbiddenException e) {
            transaction.rollback();
            throw new ForbiddenException(e);
        }
    }

}