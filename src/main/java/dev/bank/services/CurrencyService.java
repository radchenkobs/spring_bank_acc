package dev.bank.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.bank.constants.Api;
import dev.bank.entities.CurrencyExchange;
import lombok.NoArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Service
@NoArgsConstructor
public class CurrencyService {
    public List<CurrencyExchange> getCurrencyMono() throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = URI.create(Api.CURRENCY_MONO);
        ObjectMapper om = new ObjectMapper();
        ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
        CurrencyExchange[] currency = om.readValue(response.getBody(), CurrencyExchange[].class);
        return List.of(currency);
    }

}