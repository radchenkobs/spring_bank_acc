package dev.bank.helpers;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomGenerator {
    private static final Random rand = new Random();
    public static int random() {
        return rand.nextInt();
    }

    public static int random(int v) {
        return rand.nextInt(v);
    }

    public static int random(int min, int max) {
        return rand.nextInt(min, max);
    }
    public static String randomCardNumber() {
        return IntStream.range(0, 4)
                .map(el -> random(1111, 9999))
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(""));
    }
}
