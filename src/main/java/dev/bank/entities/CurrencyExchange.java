package dev.bank.entities;

public record CurrencyExchange(Number currencyCodeA,
                               Number currencyCodeB,
                               String date,
                               Double rateBuy,
                               Double rateCross,
                               Double rateSell) {
}
