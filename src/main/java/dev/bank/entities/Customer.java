package dev.bank.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dev.bank.dto.customer.RequestCustomerCreate;
import dev.bank.dto.customer.RequestCustomerUpdate;
import dev.bank.serializers.CustomerSerializer;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customer")
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize(using = CustomerSerializer.class)
public class Customer extends AbstractEntity {
    @Column(nullable = false)
    private String name;
    @Column(unique = true, nullable = false)
    private String email;
    private Integer age;
    @OneToMany(mappedBy = "customer", cascade = {CascadeType.ALL})
    @BatchSize(size = 25)
    private List<Account> accounts = new ArrayList<>();
    @ManyToMany(cascade =
            {
                    CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.REFRESH,
                    CascadeType.PERSIST
            },
            targetEntity = Employer.class)
    @JoinTable(name = "employer_customer",
            inverseJoinColumns = @JoinColumn(name = "customer_id",
                    nullable = false,
                    updatable = false),
            joinColumns = @JoinColumn(name = "employer_id",
                    nullable = false,
                    updatable = false),
            foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),
            inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
    private List<Employer> employers = new ArrayList<>();


    public Customer addAccount(Account a) {
        accounts.add(a);
        a.setCustomer(this);
        return this;
    }

    public static Customer of(RequestCustomerCreate data) {
        return Customer
                .builder()
                .name(data.getName())
                .email(data.getEmail())
                .age(data.getAge())
                .accounts(new ArrayList<>())
                .employers(new ArrayList<>())
                .build();
    }

    public static Customer of(Customer old, RequestCustomerUpdate data) {
        if (!checkNotNullOrEmpty(data.getName())) {
            old.setName(data.getName());
        }
        if (!checkNotNullOrEmpty(data.getEmail())) {
            old.setEmail(data.getEmail());
        }
        if (!checkNotNullOrEmpty(data.getAge())) {
            old.setAge(data.getAge());
        }
        return old;
    }
}
