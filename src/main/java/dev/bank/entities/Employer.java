package dev.bank.entities;

import dev.bank.dto.employer.RequestEmployerCreate;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "employer")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Employer extends AbstractEntity {
    @Column(unique = true)
    private String name;
    private String address;
    @ManyToMany(cascade =
            {
                    CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.REFRESH,
                    CascadeType.PERSIST
            },
            targetEntity = Customer.class
    )
    @JoinTable(name = "employer_customer",
            inverseJoinColumns = @JoinColumn(name = "employer_id",
                    nullable = false,
                    updatable = false),
            joinColumns = @JoinColumn(name = "customer_id",
                    nullable = false,
                    updatable = false),
            foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),
            inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
    private final List<Customer> customers = new ArrayList<>();

    public static Employer of(RequestEmployerCreate data) {
        return Employer
                .builder()
                .name(data.getName())
                .address(data.getAddress())
                .build();
    }
}
