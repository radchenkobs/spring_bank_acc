package dev.bank.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dev.bank.dto.account.RequestAccountCreate;
import dev.bank.enums.CurrencyType;
import dev.bank.exceptions.ForbiddenException;
import dev.bank.helpers.RandomGenerator;
import dev.bank.serializers.AccountSerializer;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "account")
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize(using = AccountSerializer.class)
public class Account extends AbstractEntity {
    @Column(length = 16, unique = true)
    private final String number = RandomGenerator.randomCardNumber();
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "currency_id", nullable = false, updatable = false)
    private CurrencyType currency;
    @Column(columnDefinition = "DOUBLE PRECISION")
    @ColumnDefault("0.0")
    private Double balance = 0.0d;
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Customer customer;

    public Account recharge(Double amount) {
        setBalance(getBalance() + amount);
        return this;
    }

    public Account withdraw(Double amount) throws ForbiddenException {
        if (balance < amount) {
            throw new ForbiddenException("Insufficient funds on the balance");
        }
        setBalance(getBalance() - amount);
        return this;
    }

    public static Account of(RequestAccountCreate data, Customer customer) {
        return Account
                .builder()
                .balance(0.0d)
                .currency(CurrencyType.of(data.getCurrencyId()))
                .customer(customer)
                .build();
    }
}
