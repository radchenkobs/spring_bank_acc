package dev.bank.interfaces;

import dev.bank.entities.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface Dao<T extends AbstractEntity> {

    T create(T entity);

    T update(T entity);

    default T save(T entity) {
        if (entity.getId() == null) {
            return create(entity);
        }
        return update(entity);
    }

    boolean delete(T entity);

    int deleteAll(List<T> entities);

    boolean deleteById(long id);

    int saveAll(List<T> entities);

    List<T> getAll();

    Optional<T> getById(Long id);
}
