package dev.bank.interfaces;

public interface Entity {
    Long id = null;

    default Long getId() {
        return id;
    }
}
