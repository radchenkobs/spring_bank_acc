package dev.bank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class AlreadyExistException extends RuntimeException {
    public AlreadyExistException(RuntimeException exception) {
        super(exception);
    }

    public AlreadyExistException(String message) {
        super(message);
    }
}
