package dev.bank.repo;

import dev.bank.entities.Customer;

public interface CustomerRepo extends IRepo<Customer> {
    boolean existsByEmail(String email);
}
