package dev.bank.repo;

import dev.bank.entities.Employer;

public interface EmployerRepo extends IRepo<Employer> {

    boolean existsByName(String name);
}
