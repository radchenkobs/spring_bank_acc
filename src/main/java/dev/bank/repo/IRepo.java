package dev.bank.repo;

import dev.bank.entities.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRepo<T extends AbstractEntity> extends JpaRepository<T, Long> {
}
