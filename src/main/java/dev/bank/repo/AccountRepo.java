package dev.bank.repo;

import dev.bank.entities.Account;
import org.springframework.data.jpa.repository.EntityGraph;

import java.util.Optional;


public interface AccountRepo extends IRepo<Account> {

    @Override
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"customer"})
    Optional<Account> findById(Long id);

    Optional<Account> findByNumber(String number);
}