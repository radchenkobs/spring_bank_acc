-- insert employers mock
INSERT INTO employer (name, address)
values ('employer name 1', 'employer address 1')
ON CONFLICT DO NOTHING;
INSERT INTO employer (name, address)
values ('employer name 2', 'employer address 2')
ON CONFLICT DO NOTHING;
INSERT INTO employer (name, address)
values ('employer name 3', 'employer address 3')
ON CONFLICT DO NOTHING;
INSERT INTO employer (name, address)
values ('employer name 4', 'employer address 4')
ON CONFLICT DO NOTHING;

-- insert customer mock
INSERT INTO customer (name, email, age)
values ('customer name 1', 'customer1@mail.com', 18)
ON CONFLICT DO NOTHING;
INSERT INTO customer (name, email, age)
values ('customer name 2', 'customer2@mail.com', 19)
ON CONFLICT DO NOTHING;
INSERT INTO customer (name, email, age)
values ('customer name 3', 'customer3@mail.com', 20)
ON CONFLICT DO NOTHING;

-- insert account mock
INSERT INTO account (customer_id, currency_id, number)
values (1, 0, '4444333322221111')
ON CONFLICT DO NOTHING;
INSERT INTO account (customer_id, currency_id, number)
values (1, 1, '4444333322221234')
ON CONFLICT DO NOTHING;
INSERT INTO account (customer_id, currency_id, number)
values (2, 0, '1111333322221111')
ON CONFLICT DO NOTHING;
INSERT INTO account (customer_id, currency_id, number)
values (3, 1, '1111333322221234')
ON CONFLICT DO NOTHING;

-- insert employer_customer mock
INSERT INTO employer_customer (employer_id, customer_id)
values (1, 1)
ON CONFLICT DO NOTHING;
INSERT INTO employer_customer (employer_id, customer_id)
values (1, 2)
ON CONFLICT DO NOTHING;
INSERT INTO employer_customer (employer_id, customer_id)
values (2, 1)
ON CONFLICT DO NOTHING;
INSERT INTO employer_customer (employer_id, customer_id)
values (3, 2)
ON CONFLICT DO NOTHING;
