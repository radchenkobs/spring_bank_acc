import React, {lazy} from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import {LazyLoader} from 'components';
import Root from 'modules/Root';

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);

const App = LazyLoader(lazy(() => import(/* webpackChunkName: 'App' */ './modules/App')));

root.render(
    <Root>
        <App/>
    </Root>
);
reportWebVitals();
