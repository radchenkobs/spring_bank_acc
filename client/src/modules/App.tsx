import React, {lazy} from 'react';
import Box from "@mui/material/Box";
import {LazyLoader} from "components";

const Master = LazyLoader(lazy(() => import(/* webpackChunkName: 'Master' */ './Master')));

const App = () => {
    return (
        <Box
            sx={theme => ({
                position: 'relative',
                width: '100%',
                height: '100vh',
                display: 'flex',
                flexDirection: 'column',
                overflow: "hidden",
                transition: theme.transitions.create('backgroundColor', {
                    easing: theme.transitions.easing.easeIn,
                    duration: theme.transitions.duration.enteringScreen,
                }),
            })}
        >
            <Master/>
        </Box>
    )
}

export default App;