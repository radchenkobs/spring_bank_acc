import React, {useEffect} from 'react';
import {useParams} from "react-router-dom";
import {observer} from "mobx-react";
import Container from "@mui/material/Container";
import {useStores} from "hooks";
import {Loader} from "components";
import {CustomerProfile} from "../CustomerProfile";

interface ICustomerRouteParam extends Record<string, any> {
    customerId: number;
}

export const Customer = observer(() => {
    const {customerId} = useParams<ICustomerRouteParam>();

    const {
        rootStore: {
            customerStore: {
                isLoadingGetSelectedCustomer,
                selectedCustomers,
                getSelectedCustomer,
                clearSelectedCustomer,
            }
        }
    } = useStores();

    const hasCustomerProfile = !!customerId && !!selectedCustomers;

    useEffect(() => {
        if (!!customerId) {
            getSelectedCustomer(customerId)
        }
        return () => {
            clearSelectedCustomer()
        }
    }, [customerId, getSelectedCustomer, clearSelectedCustomer])

    return (
        <Container maxWidth="lg" sx={{mt: 4, mb: 4}}>
            {isLoadingGetSelectedCustomer && <Loader/>}
            {(!isLoadingGetSelectedCustomer && hasCustomerProfile)
                ? <CustomerProfile/>
                : <div>Customer not found</div>
            }
        </Container>
    );
})
