import React from "react";
import DashboardIcon from '@mui/icons-material/Dashboard';
import PeopleIcon from '@mui/icons-material/People';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import BadgeIcon from '@mui/icons-material/Badge';
import {ACCOUNT_LIST_ROUTE, CUSTOMERS_LIST_ROUTE, EMPLOYER_LIST_ROUTE, HOME} from "constants/router";

interface INavItem {
    icon?: React.ReactElement;
    text: string;
    to: string;
}

export const navItems: INavItem[] = [
    {
        icon: <DashboardIcon/>,
        text: "Dashboard",
        to: HOME,
    },
    {
        icon: <PeopleIcon/>,
        text: "Customers",
        to: CUSTOMERS_LIST_ROUTE,
    },
    {
        icon: <AccountBalanceIcon/>,
        text: "Accounts",
        to: ACCOUNT_LIST_ROUTE,
    },
    {
        icon: <BadgeIcon/>,
        text: "Employers",
        to: EMPLOYER_LIST_ROUTE,
    },
]