import React, {useCallback, useState} from 'react';
import {Outlet} from "react-router-dom";
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from "@mui/material/IconButton";
import Divider from "@mui/material/Divider";
import List from '@mui/material/List';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import {AppBar, Drawer} from './styled';
import {navItems} from "./navList";
import {ListItemLink} from "./ListItemLink";

export const MainLayout = () => {
    const [open, setOpen] = useState(true);

    const toggleDrawer = useCallback(() => {
        setOpen(!open);
    }, [open])

    return (
        <Box sx={{display: 'flex'}}>
            <AppBar position="absolute" open={open}>
                <Toolbar
                    sx={{
                        pr: '24px', // keep right padding when drawer closed
                    }}
                >
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        onClick={toggleDrawer}
                        sx={{
                            marginRight: '36px',
                            ...(open && {display: 'none'}),
                        }}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography
                        component="h1"
                        variant="h6"
                        color="inherit"
                        noWrap
                        sx={{flexGrow: 1}}
                    >
                        Dashboard
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer variant="permanent" open={open}>
                <Toolbar
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        px: [1],
                    }}
                >
                    <IconButton onClick={toggleDrawer}>
                        <ChevronLeftIcon/>
                    </IconButton>
                </Toolbar>
                <Divider/>
                <List component="nav">
                    {navItems.map(({icon, text, to}) => (
                        <ListItemLink
                            key={to}
                            icon={icon}
                            text={text}
                            to={to}/>
                    ))}
                </List>
            </Drawer>
            <Box
                component="main"
                sx={{
                    backgroundColor: (theme) =>
                        theme.palette.mode === 'light'
                            ? theme.palette.grey[100]
                            : theme.palette.grey[900],
                    flexGrow: 1,
                    display: "flex",
                    flexDirection: "column",
                    height: '100vh',
                    overflow: 'hidden',
                }}
            >
                <Toolbar/>
                <Box
                    sx={{
                        display: "flex",
                        flexGrow: 1,
                        flexDirection: "column",
                        maxHeight: "calc(100vh - 64px)"
                    }}
                >
                    <Outlet/>
                </Box>
            </Box>
        </Box>
    );
};