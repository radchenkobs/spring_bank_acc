import React, {FC} from 'react';
import {Link} from "react-router-dom";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

interface IListItemLinkProps {
    icon?: React.ReactElement;
    text: string;
    to: string;
}


export const ListItemLink: FC<IListItemLinkProps> = ({to, icon, text}) => {
    return (
        <li>
            <ListItem button component={Link} to={to}>
                {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
                <ListItemText primary={text}/>
            </ListItem>
        </li>
    );
};