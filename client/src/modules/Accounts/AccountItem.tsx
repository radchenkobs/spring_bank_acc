import React, {FC, useCallback} from 'react';
import {generatePath, useNavigate, useParams} from "react-router-dom";
import {IAccount} from "interfaces";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import {formattedCardNumber} from 'helpers';
import {ACCOUNT_LIST_PAGE_ROUTE} from "constants/router";

interface IAccountItemProps {
    account: IAccount
}

interface IAccountRouteParam extends Record<string, any> {
    accountId: IAccount["id"];
}


export const AccountItem: FC<IAccountItemProps> = ({
                                                       account: {
                                                           id,
                                                           number,
                                                           currency,
                                                           balance,
                                                       }
                                                   }) => {
    const navigate = useNavigate();
    const {accountId} = useParams<IAccountRouteParam>();

    const handleNavigateAccount = useCallback(() => {
        navigate(generatePath(ACCOUNT_LIST_PAGE_ROUTE, {accountId: id}))
    }, [navigate, id])

    return (
        <Button onClick={handleNavigateAccount} variant="text">
            <Card
                sx={{
                    boxShadow: !!accountId && Number(accountId) === id ? "rgba(3, 102, 214, 0.3) 0px 0px 0px 3px" : "",
                }}
            >
                <CardContent>
                    <Stack spacing={1}>
                        <Typography variant="subtitle">
                            {formattedCardNumber(number)}
                        </Typography>
                        <Stack direction="row" spacing={2}>
                            <Typography>
                                {currency}
                            </Typography>
                            <Typography>
                                {balance}
                            </Typography>
                        </Stack>
                    </Stack>
                </CardContent>
            </Card>
        </Button>
    );
};