import React, {useEffect} from 'react';
import {observer} from "mobx-react";
import {useStores} from "hooks";
import {Loader} from "components";
import {AccountList} from "./AccountList";

export const Accounts = observer((() => {
    const {
        rootStore: {
            accountStore: {
                isLoadingGetAccounts,
                accounts,
                getAccounts
            }
        }
    } = useStores();

    useEffect(() => {
        getAccounts();
    }, [getAccounts])

    return (
        <div>
            {isLoadingGetAccounts && <Loader/>}
            {!!accounts.length
                ? <AccountList/>
                : <div>No accounts</div>
            }
        </div>
    );
}))