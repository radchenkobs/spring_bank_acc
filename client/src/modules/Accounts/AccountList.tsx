import React from 'react';
import {Outlet, useNavigate} from "react-router-dom";
import {observer} from "mobx-react";
import {useStores} from "hooks";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import ArrowCircleUpIcon from '@mui/icons-material/ArrowCircleUp';
import ArrowCircleDownIcon from '@mui/icons-material/ArrowCircleDown';
import CompareArrowsIcon from '@mui/icons-material/CompareArrows';
import {AccountItem} from "./AccountItem";
import {ACCOUNT_LIST_PAGE_PAY, ACCOUNT_LIST_PAGE_RECHARGE, ACCOUNT_LIST_PAGE_WITHDRAW} from "constants/router";


export const AccountList = observer(() => {
    const navigate = useNavigate();

    const {
        rootStore: {
            accountStore: {
                accounts,
            }
        }
    } = useStores();

    return (
        <Stack direction="row"
               sx={{
                   overflow: "hidden",
                   height: 'calc(100vh - 64px)'
               }}
        >
            <Box
                sx={{
                    boxShadow: 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px',
                    height: "100%",
                    overflowY: "auto",
                }}
            >
                <Box pt={4} px={2}>
                    <Typography variant="title" mb={2} align="center">
                        Accounts
                    </Typography>
                    <Stack spacing={2} mb={4}>
                        {!!accounts.length && accounts.map(account => (
                            <AccountItem account={account} key={account.id}/>
                        ))}
                    </Stack>
                </Box>
            </Box>
            <Box flexGrow={1} pt={4} px={2}>
                <Stack direction="row" spacing={3} mb={4}>
                    <Button
                        onClick={() => navigate(ACCOUNT_LIST_PAGE_RECHARGE)}
                        variant="outlined"
                        startIcon={<ArrowCircleUpIcon/>}
                    >
                        Recharge
                    </Button>
                    <Button
                        onClick={() => navigate(ACCOUNT_LIST_PAGE_WITHDRAW)}
                        variant="outlined"
                        startIcon={<ArrowCircleDownIcon/>}
                    >
                        Withdraw
                    </Button>
                    <Button
                        onClick={() => navigate(ACCOUNT_LIST_PAGE_PAY)}
                        variant="outlined"
                        startIcon={<CompareArrowsIcon/>}
                    >
                        Pay
                    </Button>
                </Stack>
                <Box
                    flexGrow={1}
                    sx={{
                        height: "100%",
                        overflow: "auto",
                    }}
                >
                    <Outlet/>
                </Box>
            </Box>
        </Stack>

    );
})