import React, {FC, useCallback, useEffect} from 'react';
import {observer} from "mobx-react";
import {useForm, useFormState} from "react-hook-form";
import {useNavigate} from "react-router-dom";
import {yupResolver} from "@hookform/resolvers/yup";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import {CustomerProfileEmployers} from "./CustomerProfileEmployers";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import {useStores} from "hooks";
import {ICustomer, ICustomerEdit} from "interfaces";
import {createSchema} from "./createSchema";
import {CustomerProfileAccounts} from "./CustomerProfileAccounts";
import {CustomerProfileFormFields} from "./CustomerProfileFormFields";

interface ICustomerProfileFormProps {
    customer: ICustomerEdit;
}

export const CustomerProfileForm: FC<ICustomerProfileFormProps> = observer(({customer}) => {
    const navigate = useNavigate();
    const {rootStore: {customerEditorStore: {saveProfile}, customerStore: {deleteCustomer}}} = useStores();

    const form = useForm<ICustomerEdit>({
        defaultValues: customer,
        values: customer,
        mode: "onBlur",
        reValidateMode: "onChange",
        // @ts-ignore
        resolver: yupResolver(createSchema),
    });

    const {control, reset} = form;

    const {isDirty, isValid} = useFormState({
        control,
    });

    const onCancel = useCallback(() => {
        if (isDirty) {
            reset()
        } else {
            navigate(-1)
        }
    }, [isDirty, reset, navigate])

    if (!customer) {
        return null;
    }

    const handleDelete = useCallback(() => {
        deleteCustomer(customer.id as ICustomer["id"], true)
    }, [deleteCustomer, customer.id])

    useEffect(() => {
        return () => {
            reset()
        }
    }, [reset])

    return (
        <form onSubmit={form.handleSubmit(saveProfile)}>
            <Stack spacing={2} sx={{height: "100%"}}>

                <Card sx={{px: 2, py: 1}}>
                    <Stack direction="row" justifyContent="space-between" spacing={3}>
                        {!!customer.id && (
                            <IconButton aria-label="delete" onClick={handleDelete}>
                                <DeleteIcon/>
                            </IconButton>
                        )}
                        <Stack direction="row" justifyContent="right" spacing={3} flexGrow={1}>
                            <Button type="submit" disabled={!isDirty || !isValid}>
                                {!customer.id ? "Create" : "Edit"}
                            </Button>
                            <Button onClick={onCancel}>
                                Cancel
                            </Button>
                        </Stack>
                    </Stack>
                </Card>

                <Card>
                    <CardContent>
                        <CustomerProfileFormFields form={form}/>
                    </CardContent>
                </Card>

                <Card>
                    <CardContent>
                        <CustomerProfileAccounts form={form}/>
                    </CardContent>
                </Card>

                <Card>
                    <CardContent>
                        <CustomerProfileEmployers form={form}/>
                    </CardContent>
                </Card>
            </Stack>
        </form>
    );
})