import React, {useEffect} from 'react';
import {observer} from "mobx-react";
import {useStores} from "hooks";
import Container from "@mui/material/Container";
import {CustomerProfileForm} from "./CustomerProfileForm";

export const CustomerProfile = observer(() => {
    const {
        rootStore: {
            employerStore: {getEmployers, isLoadingGetEmployers},
            customerStore: {selectedCustomers},
            customerEditorStore: {
                initialCustomer,
                data
            },
        }
    } = useStores();

    useEffect(() => {
        getEmployers();
        initialCustomer(selectedCustomers)
    }, [initialCustomer, selectedCustomers])

    return (
        <Container maxWidth="lg" sx={{mt: 4, mb: 4}}>
            {!isLoadingGetEmployers && (
                <CustomerProfileForm customer={data}/>
            )}
        </Container>
    );
});