import React, {FC, useCallback, useMemo} from 'react';
import {observer} from "mobx-react";
import {useFieldArray, UseFormReturn} from "react-hook-form";
import {generatePath, useNavigate} from "react-router-dom";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import DeleteIcon from '@mui/icons-material/Delete';
import AddCardIcon from '@mui/icons-material/AddCard';
import NearMeIcon from '@mui/icons-material/NearMe';
import {ICustomerEdit, ICustomerEditAccount} from "interfaces";
import {ECurrency} from "constants/ECurrency";
import {ACCOUNT_LIST_PAGE_ROUTE} from "constants/router";

interface ICustomerProfileAccountsProps {
    form: UseFormReturn<ICustomerEdit>;
}

export const CustomerProfileAccounts: FC<ICustomerProfileAccountsProps> = observer(({form}) => {
    const navigate = useNavigate()

    const {fields, append, remove, update} = useFieldArray({
        control: form.control,
        name: 'accounts',
    });

    const options = useMemo(() => {
        return Object.values(ECurrency).map(value => ({
            value,
            label: ECurrency[value],
        }));
    }, [fields])

    const onAdd = useCallback(() => {
        append({
            currency: ECurrency.UAH,
            disabled: false,
        } as ICustomerEditAccount)
    }, [append])

    const handleChange = useCallback((index: number, value: string) => {
        update(index, {
            ...fields[index],
            currency: ECurrency[value as keyof typeof ECurrency],
            label: value
        } as ICustomerEditAccount)
    }, [update, fields])

    return (
        <Stack spacing={2}>
            <Stack
                direction="row"
                spacing={2}
                justifyContent="space-between"
                alignItems="center"
            >
                <Typography>
                    Accounts
                </Typography>
                <Button onClick={onAdd}>
                    <AddCardIcon/>
                </Button>
            </Stack>
            {fields.map(({id, label, currency, disabled, idx}, index) => (
                <Stack direction="row" spacing={2} key={id}>
                    <FormControl sx={{m: 1, minWidth: 120, width: "100%"}} size="small">
                        <Select
                            fullWidth
                            disabled={disabled}
                            defaultValue={ECurrency.UAH}
                            value={label}
                            onChange={(e) => handleChange(index, e.target.value)}
                            sx={{
                                '& > svg': {
                                    display: disabled ? "none" : "inline-block",
                                }
                            }}
                        >
                            {<MenuItem value={label}>{label}</MenuItem>}
                            {options.map(({value, label}) => (
                                <MenuItem key={value} value={value}>{label}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Box sx={{display: "flex", alignItems: "center", minWidth: "40px"}}>
                        <IconButton
                            disabled={!idx}
                            onClick={() => navigate(generatePath(ACCOUNT_LIST_PAGE_ROUTE, {accountId: idx}))}
                        >
                            {!!idx && <NearMeIcon fontSize="inherit"/>}
                        </IconButton>
                    </Box>
                    <Box sx={{display: "flex", alignItems: "center"}}>
                        <IconButton
                            onClick={() => remove(index)}
                        >
                            <DeleteIcon fontSize="inherit"/>
                        </IconButton>
                    </Box>
                </Stack>
            ))}
        </Stack>
    );
})