import * as yup from 'yup';

const profileSchema = yup.object().shape({
    name: yup.string().min(2).max(200).required(),
    email: yup.string().email().required(),
    age: yup.number().min(18).max(100).required(),
});

export const createSchema = profileSchema.concat(
    yup.object().shape({
        accounts: yup.array().of(
            yup.object({
                idx: yup.number(),
                disabled: yup.boolean(),
                label: yup.string(),
                number: yup.string(),
                balance: yup.number(),
                currency: yup.string(),
            })),
    })
)