import React, {FC, useCallback, useMemo} from 'react';
import {observer} from "mobx-react";
import {useFieldArray, UseFormReturn} from "react-hook-form";
import {generatePath, useNavigate} from "react-router-dom";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import DeleteIcon from '@mui/icons-material/Delete';
import AddCardIcon from '@mui/icons-material/AddCard';
import NearMeIcon from '@mui/icons-material/NearMe';
import {ICustomerEdit} from "interfaces";
import {ACCOUNT_LIST_PAGE_ROUTE} from "constants/router";
import {ICustomerEditEmployer} from "../../interfaces/ICustomerEdit";
import {useStores} from "../../hooks";

interface ICustomerProfileAccountsProps {
    form: UseFormReturn<ICustomerEdit>;
}

export const CustomerProfileEmployers: FC<ICustomerProfileAccountsProps> = observer(({form}) => {
    const navigate = useNavigate()
    const {rootStore: {employerStore: {employers}}} = useStores();

    const {fields, append, remove, update} = useFieldArray({
        control: form.control,
        name: 'employers',
    });

    const disabledEmployers = useMemo(() => fields
        .filter(f => !!f.label)
        .map(f => f.label), [fields])

    const onAdd = useCallback(() => {
        append({} as ICustomerEditEmployer)
    }, [append])

    const handleChange = useCallback((index: number, label: string) => {
        update(index, {
            ...fields[index],
            idx: employers.find(e => e.name === label)?.id,
            label
        } as ICustomerEditEmployer)
    }, [update, fields])

    return (
        <Stack spacing={2}>
            <Stack
                direction="row"
                spacing={2}
                justifyContent="space-between"
                alignItems="center"
            >
                <Typography>
                    Employers
                </Typography>
                <Button onClick={onAdd}>
                    <AddCardIcon/>
                </Button>
            </Stack>
            {fields.map(({id, idx, label}, index) => (
                <Stack direction="row" spacing={2} key={id}>
                    <FormControl sx={{m: 1, minWidth: 120, width: "100%"}} size="small">
                        <Select
                            fullWidth
                            value={label}
                            onChange={(e) => handleChange(index, e.target.value)}
                        >
                            {employers.map(({id, name}) => (
                                <MenuItem
                                    key={id}
                                    value={name}
                                    disabled={disabledEmployers.includes(name)}
                                >
                                    {name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Box sx={{display: "flex", alignItems: "center", minWidth: "40px"}}>
                        <IconButton
                            disabled={!idx}
                            onClick={() => navigate(generatePath(ACCOUNT_LIST_PAGE_ROUTE, {accountId: idx}))}
                        >
                            {!!idx && <NearMeIcon fontSize="inherit"/>}
                        </IconButton>
                    </Box>
                    <Box sx={{display: "flex", alignItems: "center"}}>
                        <IconButton
                            onClick={() => remove(index)}
                        >
                            <DeleteIcon fontSize="inherit"/>
                        </IconButton>
                    </Box>
                </Stack>
            ))}
        </Stack>
    );
})