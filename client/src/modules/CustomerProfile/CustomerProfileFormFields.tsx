import React, {FC} from 'react';
import {Controller, UseFormReturn, useFormState} from "react-hook-form";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import {ICustomerEdit} from "interfaces";

interface ICustomerProfileFormFieldsProps {
    form: UseFormReturn<ICustomerEdit>;
}

export const CustomerProfileFormFields: FC<ICustomerProfileFormFieldsProps> = ({form}) => {
    const {control} = form;

    const {errors} = useFormState({
        control,
    });

    return (
        <Stack spacing={2}>
            <Controller
                name="email"
                control={control}
                render={({field}) =>
                    <TextField
                        {...field}
                        label="Email"
                        placeholder="Enter email"
                        error={!!errors.email?.message}
                        helperText={!!errors.email?.message && errors.email?.message}
                    />
                }
            />

            <Stack direction="row" spacing={3}>
                <Controller
                    name="name"
                    control={control}
                    render={({field}) =>
                        <TextField
                            {...field}
                            label="Name"
                            placeholder="Enter name"
                            error={!!errors.name?.message}
                            helperText={!!errors.name?.message && errors.name?.message}
                        />
                    }
                />
                <Controller
                    name="age"
                    control={control}
                    render={({field}) =>
                        <TextField
                            {...field}
                            fullWidth={false}
                            label="Age"
                            placeholder="Enter age"
                            type="number"
                            InputProps={{inputProps: {min: 1, max: 100}}}
                            error={!!errors.age?.message}
                            helperText={!!errors.age?.message && errors.age?.message}
                        />
                    }
                />
            </Stack>
        </Stack>
    );
};