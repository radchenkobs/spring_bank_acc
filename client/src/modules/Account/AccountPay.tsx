import React, {useCallback, useMemo, useState} from 'react';
import {observer} from "mobx-react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Stack from "@mui/material/Stack";
import Collapse from "@mui/material/Collapse";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {useStores} from "hooks";
import {IAccount} from "interfaces";
import {AccountAutocomplete} from "./components/AccountAutocomplete";
import {AccountNumericField} from "./components/AccountNumericField";

export const AccountPay = observer(() => {
    const {rootStore: {accountStore: {accounts, payAccount}}} = useStores();
    const [accountFrom, setAccountFrom] = useState<IAccount | null>(null);
    const [accountTo, setAccountTo] = useState<IAccount | null>(null);
    const [value, setValue] = useState(0);

    const fromOptions = useMemo(() => {
        return !accountTo
            ? accounts
            : accounts.filter(({id}) => id !== accountTo.id)
    }, [accountTo, accounts])

    const toOptions = useMemo(() => {
        return !accountFrom
            ? accounts
            : accounts.filter(({id}) => id !== accountFrom.id)
    }, [accountFrom, accounts])

    const handleChangeFromAccount = useCallback((a: IAccount | null) => setAccountFrom(a), [accountTo])

    const handleChangeToAccount = useCallback((a: IAccount | null) => setAccountTo(a), [])

    const handleTransfer = useCallback(() => {
        if (!accountFrom || !accountTo || !value) return;
        payAccount({from: accountFrom, to: accountTo, amount: value});
    }, [payAccount, accountFrom, accountTo, value])

    return (
        <Box pt={2}>
            <Card>
                <CardContent>
                    <Stack spacing={4}>
                        <AccountAutocomplete
                            options={fromOptions}
                            onChange={handleChangeFromAccount}
                            label="Account from"
                        />
                        <Collapse in={!!accountFrom}>
                            <AccountAutocomplete
                                options={toOptions}
                                onChange={handleChangeToAccount}
                                label="Account to"
                            />
                        </Collapse>
                        <Collapse in={!!accountFrom && !!accountTo}>
                            <AccountNumericField
                                value={value}
                                onChange={(v) => setValue(v)}
                            />
                        </Collapse>
                        <Collapse in={!!accountFrom && !!accountTo && !!value}>
                            <Button
                                onClick={handleTransfer}
                                disabled={!accountFrom || !accountTo || accountFrom.balance < value}
                                fullWidth
                            >
                                Pay
                            </Button>
                        </Collapse>
                    </Stack>
                </CardContent>
            </Card>
        </Box>
    );
});