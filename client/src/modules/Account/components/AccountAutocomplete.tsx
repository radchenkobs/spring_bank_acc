import React, {FC, SyntheticEvent, useCallback, useEffect, useState} from 'react';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Typography from "@mui/material/Typography";

import Collapse from '@mui/material/Collapse';
import {useStores} from "hooks";
import {IAccount} from "interfaces";
import {formattedCardNumber} from "helpers";
import {ECurrencySymbol} from "constants/ECurrency";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import MenuItem from "@mui/material/MenuItem";

interface IRenderInnerProps {
    account: IAccount
}

interface IAccountAutocompleteProps {
    options: IAccount[];
    label?: string
    onChange: (value: IAccount | null) => void;
    value?: IAccount | null;
}

const RenderInner: FC<IRenderInnerProps> = ({account}) => {
    const {number, currency, balance} = account;
    return (
        <Stack spacing={2} ml={1}>
            <Stack direction="row" spacing={1}>
                <Typography variant="subtitle">
                    {currency}
                </Typography>
                <Typography variant="subtitle">
                    {formattedCardNumber(number)}
                </Typography>
            </Stack>
            <Stack direction="row" spacing={1}>
                <Typography>
                    {ECurrencySymbol[currency]}
                </Typography>
                <Typography>
                    {balance}
                </Typography>
            </Stack>
        </Stack>
    )
}

export const AccountAutocomplete: FC<IAccountAutocompleteProps> = ({
                                                                       options,
                                                                       label,
                                                                       onChange,
                                                                       value: inValue
                                                                   }) => {
    const {rootStore: {accountStore: {accountsMap, accounts}}} = useStores();
    const [value, setValue] = useState<IAccount | null>(inValue || null);
    const renderOptionLabel = ({number, customer: {name}}: IAccount) => `${number} ${name}`

    const handleChange = useCallback((event: SyntheticEvent, value: IAccount | null) => {
        if (!value || !value.id) {
            setValue(null);
            return;
        }
        setValue(accountsMap.get(value.id) as IAccount)
    }, []);

    const renderOptionNode = (props: object, option: IAccount) => {
        const {number, currency, balance, customer: {name}} = option;
        const num = formattedCardNumber(number);
        const bal = `${ECurrencySymbol[currency]} ${balance.toFixed(2)}`
        return (
            <MenuItem {...props} key={option.id} value={option.number}>
                {num} - {bal} - {name}
            </MenuItem>
        )
    }

    useEffect(() => {
        onChange(value);
    }, [onChange, value])

    useEffect(() => {
        if (!!value) {
            setValue(accounts.find(({id}) => value.id === id) || null);
        }
    }, [accounts, value])

    useEffect(() => {
        if (inValue !== undefined) {
            setValue(inValue)
        }
    }, [inValue])

    return (
        <Stack spacing={3}>
            <Box>
                <Autocomplete
                    disablePortal
                    options={options}
                    onChange={handleChange}
                    getOptionLabel={renderOptionLabel}
                    renderOption={renderOptionNode}
                    fullWidth
                    isOptionEqualToValue={(opt, v) => opt.id === v.id}
                    renderInput={(params) => <TextField {...params} label={label ?? "Account"}/>}
                />
            </Box>
            <Collapse in={!!value}>
                {value && <RenderInner account={value}/>}
            </Collapse>
        </Stack>
    );
};