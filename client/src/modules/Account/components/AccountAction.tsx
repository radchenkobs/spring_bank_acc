import React, {FC, useCallback, useState} from 'react';
import {observer} from "mobx-react";
import Box from '@mui/material/Box';
import Stack from "@mui/material/Stack";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Collapse from "@mui/material/Collapse";
import Button from "@mui/material/Button";
import {useStores} from "hooks";
import {IAccount} from "interfaces";
import {AccountAutocomplete} from "./AccountAutocomplete";
import {AccountNumericField, IAccountNumericFieldProps} from "./AccountNumericField";

interface IAccountActionProps {
    onClick: (account: IAccount, amount: number) => {},
    inputProps?: Partial<IAccountNumericFieldProps>;
    onAccount?: (account: IAccount | null) => void;
}

export const AccountAction: FC<IAccountActionProps> = observer((props) => {
    const {
        onClick,
        inputProps = {} as IAccountNumericFieldProps,
        onAccount
    } = props;
    const {rootStore: {accountStore: {accounts}}} = useStores();
    const [account, setAccount] = useState<IAccount | null>(null);
    const [value, setValue] = useState(0);

    const handleChangeAccount = useCallback((v: IAccount | null) => {
        setAccount(v)
        onAccount && onAccount(v)
    }, [onAccount])

    const handleApply = useCallback(() => {
        if (!!account) {
            onClick(account, value)
        }
    }, [onClick, account, value])

    return (
        <Box pt={2}>
            <Card>
                <CardContent>
                    <Stack spacing={2}>
                        <AccountAutocomplete
                            options={accounts}
                            onChange={handleChangeAccount}
                        />
                        <Collapse in={!!account}>
                            <Stack spacing={2}>
                                <AccountNumericField
                                    {...inputProps}
                                    value={value}
                                    onChange={(v) => setValue(v)}
                                />
                                <Box>
                                    <Button onClick={handleApply} disabled={!account || !value}>
                                        button
                                    </Button>
                                </Box>
                            </Stack>
                        </Collapse>
                    </Stack>
                </CardContent>
            </Card>
        </Box>
    );
});