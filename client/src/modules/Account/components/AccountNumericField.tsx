import React, {FC} from 'react';
import TextField from "@mui/material/TextField";

export interface IAccountNumericFieldProps {
    value: number;
    onChange: (value: number) => void;
    min?: number;
    max?: number;
}

export const AccountNumericField: FC<IAccountNumericFieldProps> = (props) => {
    const {value, onChange, min = 0, max} = props;
    return (
        <TextField
            size="small"
            type="number"
            value={value}
            onChange={e => onChange(Number(e.target.value))}
            InputProps={{
                inputProps: {
                    min: min,
                    max: max,
                    inputMode: "numeric",
                    pattern: '[0-9]*',
                },
            }}
        />
    );
};