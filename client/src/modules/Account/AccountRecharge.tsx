import React from 'react';
import {observer} from "mobx-react";
import {useStores} from "hooks";
import {AccountAction} from "./components/AccountAction";

export const AccountRecharge = observer(() => {
    const {rootStore: {accountStore: {rechargeAccount}}} = useStores();

    return (
        <AccountAction onClick={({number}, amount) => rechargeAccount(number, amount)}/>
    );
});