import React, {useCallback, useState} from 'react';
import {observer} from "mobx-react";
import {useStores} from "hooks";
import {IAccount} from "interfaces";
import {AccountAction} from "./components/AccountAction";

const DEFAULT_MAX_INPUT_VALUE = 99999

export const AccountWithdraw = observer(() => {
    const {rootStore: {accountStore: {withdrawAccount}}} = useStores();
    const [maxInputValue, setMaxInputValue] = useState(DEFAULT_MAX_INPUT_VALUE);

    const handleAccount = useCallback((account: IAccount | null) => {
        if (!account) {
            setMaxInputValue(DEFAULT_MAX_INPUT_VALUE);
            return;
        }
        setMaxInputValue(account.balance);
    }, [])

    return (
        <AccountAction
            onClick={({number}, amount) => withdrawAccount(number, amount)}
            onAccount={handleAccount}
            inputProps={{max: maxInputValue}}
        />
    );
});