import React, {useMemo} from 'react';
import {observer} from "mobx-react";
import {Navigate, useParams} from "react-router-dom";
import {useStores} from "hooks";
import {IAccount} from "interfaces";
import {ACCOUNT_LIST_ROUTE} from 'constants/router'
import {AccountProfileCard} from './AccountProfileCard';

interface IAccountRouteParam extends Record<string, any> {
    accountId: IAccount["id"];
}


export const AccountProfile = observer(() => {
    const {accountId} = useParams<IAccountRouteParam>();
    const {rootStore: {accountStore: {accountsMap}}} = useStores();

    const id = useMemo(() => Number(accountId), [accountId])

    if (!id || !accountsMap.size || !accountsMap.has(id)) {
        return <Navigate to={ACCOUNT_LIST_ROUTE}/>
    }

    return (
        <AccountProfileCard account={accountsMap.get(id) as IAccount}/>
    );
})