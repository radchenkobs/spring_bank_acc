import React, {FC, useCallback, useEffect, useState} from 'react';
import {generatePath, useNavigate} from "react-router-dom";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import ArrowCircleUpIcon from '@mui/icons-material/ArrowCircleUp';
import ArrowCircleDownIcon from '@mui/icons-material/ArrowCircleDown';
import TextField from "@mui/material/TextField";
import {useStores} from "hooks";
import {IAccount} from "interfaces";
import {formattedCardNumber} from "helpers";
import {CUSTOMERS_PAGE_ROUTE} from "constants/router";
import {AccountField} from "./AccountField";

interface IAccountProfileCardProps {
    account: IAccount
}

export const AccountProfileCard: FC<IAccountProfileCardProps> = ({
                                                                     account: {
                                                                         id,
                                                                         number,
                                                                         currency,
                                                                         balance,
                                                                         customer: {
                                                                             id: customerId,
                                                                             name,
                                                                             email,
                                                                             age,
                                                                         }
                                                                     }
                                                                 }) => {
    const navigate = useNavigate();
    const {
        rootStore: {
            accountStore: {
                deleteAccount,
                rechargeAccount,
                withdrawAccount
            }
        }
    } = useStores();
    const [rechargeValue, setRechargeValue] = useState(0);
    const [withdrawValue, setWithdrawValue] = useState(balance);

    const handleNavigateCustomer = useCallback(() => {
        navigate(generatePath(CUSTOMERS_PAGE_ROUTE, {customerId: customerId}))
    }, [navigate, customerId])

    const handleDelete = useCallback(() => {
        deleteAccount(id);
    }, [deleteAccount, id])

    const handleRecharge = useCallback(async () => {
        await rechargeAccount(number, rechargeValue);
        setRechargeValue(0)
    }, [rechargeValue, rechargeAccount])

    const handleWithdraw = useCallback(() => {
        withdrawAccount(number, withdrawValue);
    }, [withdrawValue, withdrawAccount])

    useEffect(() => {
        setWithdrawValue(balance)
    }, [balance])

    return (
        <Card>
            <CardContent>
                <Stack spacing={3}>
                    <AccountField
                        title="Account"
                        onClick={handleDelete}
                        buttonIcon={<DeleteOutlineIcon/>}
                    >
                        <Stack spacing={1} flexGrow={1}>
                            <Typography variant="subtitle">
                                {formattedCardNumber(number)}
                            </Typography>
                            <Stack direction="row" spacing={2}>
                                <Typography>
                                    {currency}
                                </Typography>
                                <Typography>
                                    {balance}
                                </Typography>
                            </Stack>
                        </Stack>
                    </AccountField>

                    <AccountField
                        title="Recharge"
                        onClick={handleRecharge}
                        buttonIcon={<ArrowCircleUpIcon/>}
                    >
                        <TextField
                            size="small"
                            type="number"
                            value={rechargeValue}
                            onChange={e => setRechargeValue(Number(e.target.value))}
                            InputProps={{
                                inputProps: {
                                    min: 0,
                                    inputMode: "numeric",
                                    pattern: '[0-9]*',
                                },
                            }}
                        />
                    </AccountField>

                    <AccountField
                        title="Withdraw"
                        onClick={handleWithdraw}
                        buttonIcon={<ArrowCircleDownIcon/>}
                    >
                        <TextField
                            size="small"
                            type="number"
                            value={withdrawValue}
                            onChange={e => setWithdrawValue(Number(e.target.value))}
                            InputProps={{
                                inputProps: {
                                    min: 0,
                                    max: balance,
                                    inputMode: "numeric",
                                    pattern: '[0-9]*',
                                },
                            }}
                        />
                    </AccountField>


                    <AccountField
                        title="Customer"
                        onClick={handleNavigateCustomer}
                        buttonIcon={<PersonOutlineIcon/>}
                    >
                        <Stack spacing={1} flexGrow={1}>
                            <Stack spacing={1} direction="row">
                                <Typography>Name:</Typography>
                                <Typography>{name}</Typography>
                            </Stack>

                            <Stack spacing={1} direction="row">
                                <Typography>Email:</Typography>
                                <Typography>{email}</Typography>
                            </Stack>

                            <Stack spacing={1} direction="row">
                                <Typography>Age:</Typography>
                                <Typography>{age}</Typography>
                            </Stack>

                        </Stack>
                    </AccountField>

                </Stack>
            </CardContent>
        </Card>
    );
};