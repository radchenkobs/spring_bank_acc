import React, {FC, ReactNode} from 'react';
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

interface IAccountFieldsProps {
    title: string;
    children: ReactNode;
    onClick?: () => void;
    buttonIcon?: ReactNode;
}

export const AccountField: FC<IAccountFieldsProps> = ({title, children, onClick, buttonIcon: Icon}) => {
    return (
        <Stack direction="row" spacing={2}>
            <Typography variant="subtitle" minWidth="100px">
                {title}
            </Typography>

            <Stack spacing={2} flexGrow={1} direction="row" justifyContent="stace-between">
                <Stack flexGrow={1}>
                    {children}
                </Stack>

                {onClick && (
                    <Box>
                        <Button onClick={onClick}>{Icon}</Button>
                    </Box>
                )}
            </Stack>
        </Stack>
    );
};