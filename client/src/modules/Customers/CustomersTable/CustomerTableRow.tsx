import React, {FC, useCallback} from 'react';
import {generatePath, useNavigate} from "react-router-dom";
import IconButton from '@mui/material/IconButton';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {CUSTOMERS_PAGE_ROUTE} from "constants/router";
import {ICustomer} from "interfaces";
import {useStores} from "hooks";
import {CustomerTableAccountsRow} from "./CustomerTableAccountsRow";
import {CustomerTableEmployersRow} from "./CustomerTableEmployersRow";

interface ICustomerTableRowProps {
    customer: ICustomer
}

export const CustomerTableRow: FC<ICustomerTableRowProps> = ({
                                                                 customer: {
                                                                     id,
                                                                     name,
                                                                     email,
                                                                     age,
                                                                     accounts,
                                                                     employers
                                                                 }
                                                             }) => {
    const [open, setOpen] = React.useState(false);
    const {
        rootStore: {
            customerStore: {deleteCustomer}
        }
    } = useStores();
    const navigate = useNavigate();

    const handleNavigateCustomer = useCallback(() => {
        navigate(generatePath(CUSTOMERS_PAGE_ROUTE, {customerId: id}))
    }, [navigate, id])

    const handleDelete = useCallback(() => {
        deleteCustomer(id);
    }, [deleteCustomer, id])

    return (
        <>
            <TableRow sx={{'& > *': {borderBottom: 'unset'}}}>
                <TableCell>
                    <IconButton
                        disabled={!accounts?.length && !employers?.length}
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {name}
                </TableCell>
                <TableCell>{email}</TableCell>
                <TableCell align="right">{age}</TableCell>
                <TableCell align="right">{accounts?.length}</TableCell>
                <TableCell align="right">{employers?.length}</TableCell>
                <TableCell align="right">
                    <IconButton aria-label="edit" onClick={handleNavigateCustomer}>
                        <EditIcon/>
                    </IconButton>
                </TableCell>
                <TableCell align="right">
                    <IconButton aria-label="delete" onClick={handleDelete}>
                        <DeleteIcon/>
                    </IconButton>
                </TableCell>
            </TableRow>
            {!!accounts?.length && (
                <CustomerTableAccountsRow open={open} accounts={accounts}/>
            )}
            {!!employers?.length && (
                <CustomerTableEmployersRow open={open} employers={employers}/>
            )}
        </>
    );
};