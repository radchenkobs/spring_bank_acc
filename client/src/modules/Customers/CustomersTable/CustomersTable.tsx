import React, {FC, ReactElement} from 'react';
import Paper from "@mui/material/Paper";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";

interface ICustomersTableProps {
    children: ReactElement
}

const CustomerTableHead = () => {
    return (
        <TableHead>
            <TableRow>
                <TableCell/>
                <TableCell>Name</TableCell>
                <TableCell>Email</TableCell>
                <TableCell align="right">Age</TableCell>
                <TableCell align="right">Accounts</TableCell>
                <TableCell align="right">Employers</TableCell>
                <TableCell align="right">Edit</TableCell>
                <TableCell align="right">Delete</TableCell>
            </TableRow>
        </TableHead>
    )
}

export const CustomersTable: FC<ICustomersTableProps> = ({children}) => {
    return (
        <Paper>
            <TableContainer>
                <Table size="small" stickyHeader>
                    <CustomerTableHead/>
                    <TableBody sx={{flexGrow: 1}}>
                        {children}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25, {label: 'All', value: -1}]}
                                colSpan={3}
                                count={100}
                                rowsPerPage={10}
                                page={0}
                                SelectProps={{
                                    inputProps: {
                                        'aria-label': 'rows per page',
                                    },
                                    native: true,
                                }}
                                onPageChange={() => {
                                }}
                                onRowsPerPageChange={() => {
                                }}
                                // ActionsComponent={TablePaginationActions}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Paper>
    );
}