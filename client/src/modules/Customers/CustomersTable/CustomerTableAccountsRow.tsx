import React, {FC, useCallback} from 'react';
import {generatePath, useNavigate} from "react-router-dom";
import TableCell from "@mui/material/TableCell";
import Collapse from "@mui/material/Collapse";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import IconButton from "@mui/material/IconButton";
import EditIcon from "@mui/icons-material/Edit";
import {formattedCardNumber} from "helpers";
import {IAccount} from "interfaces";
import {ACCOUNT_LIST_PAGE_ROUTE} from "constants/router";

interface ICustomerTableAccountRowProps {
    open: boolean;
    accounts: IAccount[]
}

export const CustomerTableAccountsRow: FC<ICustomerTableAccountRowProps> = ({
                                                                                open,
                                                                                accounts,
                                                                            }) => {
    const navigate = useNavigate();

    const handleNavigateAccount = useCallback((id: IAccount["id"]) => {
        navigate(generatePath(ACCOUNT_LIST_PAGE_ROUTE, {accountId: id}))
    }, [navigate])

    return (
        <TableRow>
            <TableCell style={{paddingBottom: 0, paddingTop: 0, borderBottom: "unset"}} colSpan={6}>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <Box sx={{margin: 1}}>
                        <Typography variant="h6" gutterBottom component="div">
                            Accounts
                        </Typography>
                        <Table size="small" aria-label="purchases">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Number</TableCell>
                                    <TableCell align="right">Currency</TableCell>
                                    <TableCell align="right">Balance</TableCell>
                                    <TableCell align="right">Edit</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {accounts.map((account) => (
                                    <TableRow key={account.id}>
                                        <TableCell component="th" scope="row">
                                            {formattedCardNumber(account.number)}
                                        </TableCell>
                                        <TableCell align="right">{account.currency}</TableCell>
                                        <TableCell align="right">{account.balance}</TableCell>
                                        <TableCell align="right">
                                            <IconButton aria-label="edit"
                                                        onClick={() => handleNavigateAccount(account.id)}>
                                                <EditIcon/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Box>
                </Collapse>
            </TableCell>
        </TableRow>
    )

};