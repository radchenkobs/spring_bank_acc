import React, {FC, useCallback} from 'react';
import {generatePath, useNavigate} from "react-router-dom";
import TableCell from "@mui/material/TableCell";
import Collapse from "@mui/material/Collapse";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import IconButton from "@mui/material/IconButton";
import EditIcon from "@mui/icons-material/Edit";
import {IEmployer} from "interfaces";
import {EMPLOYER_LIST_ROUTE_PAGE} from "constants/router";

interface ICustomerTableAccountRowProps {
    open: boolean;
    employers: IEmployer[]
}

export const CustomerTableEmployersRow: FC<ICustomerTableAccountRowProps> = ({
                                                                                 open,
                                                                                 employers,
                                                                             }) => {
    const navigate = useNavigate();

    const handleNavigateEmployer = useCallback((id: IEmployer["id"]) => {
        navigate(generatePath(EMPLOYER_LIST_ROUTE_PAGE, {employerId: id}))
    }, [navigate])

    return (
        <TableRow>
            <TableCell style={{paddingBottom: 0, paddingTop: 0, borderBottom: "unset"}} colSpan={6}>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <Box sx={{margin: 1}}>
                        <Typography variant="h6" gutterBottom component="div">
                            Employers
                        </Typography>
                        <Table size="small" aria-label="purchases">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell align="right">Address</TableCell>
                                    <TableCell align="right">Edit</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {employers.map(({id, name, address}) => (
                                    <TableRow key={id}>
                                        <TableCell component="th" scope="row">
                                            {name}
                                        </TableCell>
                                        <TableCell align="right">{address}</TableCell>
                                        <TableCell align="right">
                                            <IconButton aria-label="edit"
                                                        onClick={() => handleNavigateEmployer(id)}>
                                                <EditIcon/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Box>
                </Collapse>
            </TableCell>
        </TableRow>
    )

};