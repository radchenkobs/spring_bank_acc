import React, {useCallback} from 'react';
import {useNavigate} from "react-router-dom";
import {observer} from "mobx-react";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from "@mui/material/Button";
import CreateIcon from '@mui/icons-material/Create';
import Stack from "@mui/material/Stack";
import {useStores} from "hooks";
import {CUSTOMERS_CREATE_ROUTE} from "constants/router";
import {CustomerTableRow} from './CustomersTable/CustomerTableRow';
import {ITableHeaderOption, Table} from 'components';

const tableHeadOptions: ITableHeaderOption[] = [
    {label: "Name"},
    {label: "Email"},
    {label: "Age", align: "right"},
    {label: "Accounts", align: "right"},
    {label: "Employers", align: "right"},
    {label: "Edit", align: "right"},
    {label: "Delete", align: "right"},
]

export const CustomerList = observer(() => {
    const {
        rootStore: {
            customerStore: {
                customers
            }
        }
    } = useStores();

    const navigate = useNavigate();

    const handleNavigateCreate = useCallback(() => {
        navigate(CUSTOMERS_CREATE_ROUTE)
    }, [navigate])

    return (
        <Stack spacing={0} height="100%" overflow="hidden">
            <Stack direction="row" justifyContent="space-between" alignItems="center" py={2}>
                <Typography variant="subtitle" align="center">
                    Customers
                </Typography>
                <Box>
                    <Button
                        onClick={handleNavigateCreate}
                        size="small"
                        startIcon={<CreateIcon/>}
                    >
                        Create
                    </Button>
                </Box>
            </Stack>

            <Box flexGrow={1} maxHeight="100%" height="100%">
                <Table headerOptions={tableHeadOptions}>
                    {!!customers.length && customers.map(customer => (
                        <CustomerTableRow key={customer.id} customer={customer}/>
                    ))}
                </Table>
            </Box>
        </Stack>

    );
})