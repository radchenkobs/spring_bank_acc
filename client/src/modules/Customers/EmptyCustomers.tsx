import React, {useCallback} from 'react';
import {useNavigate} from "react-router-dom";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {CUSTOMERS_CREATE_ROUTE} from "constants/router";

export const EmptyCustomers = () => {
    const navigate = useNavigate();

    const handleNavigateCreate = useCallback(() => {
        navigate(CUSTOMERS_CREATE_ROUTE)
    }, [navigate])

    return (
        <Stack justifyContent="center" alignItems="center" spacing={2}>
            <Typography variant="title">No customers yet</Typography>
            <Box>
                <Button size="small" onClick={handleNavigateCreate}>Create</Button>
            </Box>
        </Stack>
    );
};