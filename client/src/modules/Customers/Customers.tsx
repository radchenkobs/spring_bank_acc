import React, {useEffect} from 'react';
import {observer} from "mobx-react";
import Box from "@mui/material/Box";
import {useStores} from "hooks";
import {Loader} from "components";
import {CustomerList} from "./CustomerList";
import {EmptyCustomers} from "./EmptyCustomers";

export const Customers = observer((() => {
    const {
        rootStore: {
            customerStore: {
                isLoadingGetCustomers,
                customersMap,
                getCustomers
            }
        }
    } = useStores();

    useEffect(() => {
        getCustomers();
    }, [getCustomers])

    return (
        <Box px={4} maxHeight="100%" height="100%">
            {isLoadingGetCustomers && <Loader/>}
            {!!customersMap.size
                ? <CustomerList/>
                : <EmptyCustomers/>
            }
        </Box>
    );
}))