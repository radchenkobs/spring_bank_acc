import React, {useEffect} from 'react';
import {observer} from "mobx-react";
import {CssBaseline, ThemeProvider} from "@mui/material";
import {unstable_HistoryRouter as HistoryRouter} from 'react-router-dom';
import {theme} from "theme";
import {useStores} from "hooks";

export const Root = observer(({children}: any): JSX.Element => {
    const {
        rootStore: {
            history,
            currencyStore: {
                getCurrencies
            }
        }
    } = useStores();


    useEffect(() => {
        getCurrencies();
    }, [])

    return (
        <HistoryRouter history={history as any} basename={"/"}>
            <ThemeProvider theme={theme}>
                <CssBaseline/>
                {children}
            </ThemeProvider>
        </HistoryRouter>
    );
})

export default Root;