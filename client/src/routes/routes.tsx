import {lazy} from "react";
import {Outlet, RouteObject} from 'react-router-dom';
import {LazyLoader} from "components";
import {MainLayout} from "modules/MainLayout";
import {
    ACCOUNT_LIST_PAGE_PAY,
    ACCOUNT_LIST_PAGE_RECHARGE,
    ACCOUNT_LIST_PAGE_ROUTE,
    ACCOUNT_LIST_PAGE_WITHDRAW,
    ACCOUNT_LIST_ROUTE,
    CUSTOMERS_CREATE_ROUTE,
    CUSTOMERS_LIST_ROUTE,
    CUSTOMERS_PAGE_ROUTE,
    EMPLOYER_LIST_ROUTE,
    EMPLOYER_LIST_ROUTE_PAGE
} from 'constants/router';

const NotFound = LazyLoader(lazy(() => import(/* webpackChunkName: 'NotFound' */ 'modules/NotFound')));

const Customers = LazyLoader(lazy(() => import(/* webpackChunkName: 'Customers' */ 'modules/Customers')));
const Customer = LazyLoader(lazy(() => import(/* webpackChunkName: 'Customer' */ 'modules/Customer')));
const CustomerProfile = LazyLoader(lazy(() => import(/* webpackChunkName: 'CustomerProfile' */ 'modules/CustomerProfile')));

const Accounts = LazyLoader(lazy(() => import(/* webpackChunkName: 'Accounts' */ 'modules/Accounts')));
const AccountProfile = LazyLoader(
    lazy(() => import(/* webpackChunkName: 'AccountProfile' */ 'modules/Account').then(module => ({default: module.AccountProfile}))),
);
const AccountRecharge = LazyLoader(
    lazy(() => import(/* webpackChunkName: 'AccountRecharge' */ 'modules/Account').then(module => ({default: module.AccountRecharge}))),
);
const AccountWithdraw = LazyLoader(
    lazy(() => import(/* webpackChunkName: 'AccountWithdraw' */ 'modules/Account').then(module => ({default: module.AccountWithdraw}))),
);
const AccountPay = LazyLoader(
    lazy(() => import(/* webpackChunkName: 'AccountPay' */ 'modules/Account').then(module => ({default: module.AccountPay}))),
);


export const routes: RouteObject[] = [
    {
        path: '/',
        element: <Outlet/>,
        children: [
            {
                element: <MainLayout/>,
                children: [
                    {
                        index: true,
                        element: <div>Index element "/"</div>
                    },
                    {
                        path: CUSTOMERS_LIST_ROUTE,
                        element: <Outlet/>,
                        children: [
                            {
                                index: true,
                                element: <Customers/>,
                            },
                            {
                                path: CUSTOMERS_CREATE_ROUTE,
                                element: <CustomerProfile/>,
                            },
                            {
                                path: CUSTOMERS_PAGE_ROUTE,
                                element: <Customer/>,
                            },
                        ],
                    },
                    {
                        path: ACCOUNT_LIST_ROUTE,
                        element: <Outlet/>,
                        children: [
                            {
                                // index: true,
                                path: ACCOUNT_LIST_ROUTE,
                                element: <Accounts/>,
                                children: [
                                    {
                                        path: ACCOUNT_LIST_PAGE_ROUTE,
                                        element: <AccountProfile/>,
                                    },
                                    {
                                        path: ACCOUNT_LIST_PAGE_RECHARGE,
                                        element: <AccountRecharge/>,
                                    },
                                    {
                                        path: ACCOUNT_LIST_PAGE_WITHDRAW,
                                        element: <AccountWithdraw/>,
                                    },
                                    {
                                        path: ACCOUNT_LIST_PAGE_PAY,
                                        element: <AccountPay/>,
                                    },
                                ]
                            },
                        ],
                    },
                    {
                        path: EMPLOYER_LIST_ROUTE,
                        element: <Outlet/>,
                        children: [
                            {
                                index: true,
                                element: <div>Employers index</div>,
                            },
                            {
                                path: EMPLOYER_LIST_ROUTE_PAGE,
                                element: <div>Employer page</div>,
                            },
                        ],
                    },
                ],
            },
            {path: '*', element: <NotFound/>},
        ],
    },
];