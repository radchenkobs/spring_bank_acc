import {createTheme} from "@mui/material";

declare module '@mui/material/styles' {
    interface TypographyVariants {
        title: React.CSSProperties;
        subtitle: React.CSSProperties;
        body: React.CSSProperties;
    }

    interface TypographyVariantsOptions {
        title?: React.CSSProperties;
        subtitle?: React.CSSProperties;
        body?: React.CSSProperties;
    }

}

declare module '@mui/material/Typography' {
    interface TypographyPropsVariantOverrides {
        title: true;
        subtitle: true;
        body: true;
    }
}

export const theme = createTheme({
    typography: {
        title: {
            fontSize: '40px',
            lineHeight: '40px',
            fontWeight: 400,
        },
        subtitle: {
            fontSize: '20px',
            lineHeight: '20px',
            fontWeight: 400,
        },
        body: {
            fontSize: '16px',
            lineHeight: '26px',
            fontWeight: 400,
        },
    },
    components: {
        MuiTextField: {
            defaultProps: {
                variant: "outlined",
                fullWidth: true,
            }
        },
        MuiButton: {
            defaultProps: {
                variant: "contained",
            },
            styleOverrides: {}
        },
        MuiTypography: {
            defaultProps: {
                variant: 'body',
                variantMapping: {
                    title: 'h2',
                    subtitle: 'h4',
                    body: 'span',
                },
            },
        },
    }
});