import {matchPath, PathPattern, useLocation} from "react-router-dom";

export const usePathPattern = (routes: string) => {

    const {pathname} = useLocation();

    return matchPath({path: routes} as PathPattern, pathname);
}