import {ICustomer} from "./ICustomer";
import {IAccount} from "./IAccount";
import {ECurrency} from "../constants/ECurrency";
import {IEmployer} from "./IEmployer";

export interface ICustomerEditAccount extends Omit<IAccount, "id"> {
    idx: number;
    disabled: boolean;
    label: string;
}

export interface ICustomerEditEmployer extends Omit<IEmployer, "id"> {
    idx: number;
    label: string;
}

export interface ICustomerEdit extends Partial<Omit<ICustomer, "age" | "accounts" | "employers">> {
    age: number | string;
    accounts: ICustomerEditAccount[];
    employers: ICustomerEditEmployer[];
}

export interface ICustomerCreateData extends Omit<ICustomerEdit, "accounts" | "age" | "employers"> {
    age: number,
    currencies?: ECurrency[];
    employers?: IEmployer["id"][];
}