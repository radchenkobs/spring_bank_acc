import {ICurrency} from "./ICurrency";

export interface ICurrencyStore {
    currenciesMap: Map<ICurrency["id"], ICurrency>;
    currencies: ICurrency[];

    isLoadingCurrencies: boolean;

    getCurrencies(): void;

    convertFromValueToId(value: string): number;
}