import {IAsyncStatus} from "./IAsyncStatus";

export interface IAsyncKey extends Symbol {
    asyncKey: true;
}

export interface IAsyncStore {
    createKey(stringKey: string): IAsyncKey;

    setLoading(key: IAsyncKey): void;

    setSuccess(key: IAsyncKey): void;

    setError(key: IAsyncKey, message?: string): void;

    getAsyncStatus(key: IAsyncKey): IAsyncStatus;

}