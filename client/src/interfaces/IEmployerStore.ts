import {IEmployer} from "./IEmployer";

export interface IEmployerStore {
    employersMap: Map<IEmployer["id"], IEmployer>;
    employers: IEmployer[];

    isLoadingGetEmployers: boolean;

    getEmployers(): void;
}