import {ICustomer} from "./ICustomer";
import {ICustomerEdit} from "./ICustomerEdit";

export interface ICustomerEditorStore {
    data: ICustomerEdit;

    initialCustomer(customer: ICustomer | null): void;

    saveProfile(data: ICustomerEdit): void;
}