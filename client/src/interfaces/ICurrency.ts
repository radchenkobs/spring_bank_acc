export interface ICurrency {
    id: number,
    value: string;
    symbol: string;
    code: number;
}