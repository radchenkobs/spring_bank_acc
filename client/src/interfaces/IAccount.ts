import {ECurrency} from "constants/ECurrency";
import {ICustomer} from "./ICustomer";
import {ICurrency} from "./ICurrency";

export interface IAccount {
    id: number;
    number: string;
    balance: number;
    currency: ECurrency;
    customer: ICustomer;
    created_at: number,
}

export interface ICreateAccountData {
    customerId: ICustomer["id"];
    currencyId: ICurrency["id"];
}