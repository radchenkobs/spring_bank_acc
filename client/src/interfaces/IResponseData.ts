export interface IData {
    message?: string;
}

export interface IResponseData<T> {
    success: boolean;
    data: IData & T
}