import {IAccount} from "./IAccount";
import {IEmployer} from "./IEmployer";

export interface ICustomer {
    id: number,
    name: string;
    email: string;
    age: number;
    accounts?: IAccount[];
    employers?: IEmployer[];
}