import {ICustomer} from "./ICustomer";

export interface ICustomerStore {
    customersMap: Map<ICustomer["id"], ICustomer>;
    customers: ICustomer[];
    selectedCustomers: ICustomer | null;

    isLoadingGetCustomers: boolean;
    isLoadingGetSelectedCustomer: boolean;
    isLoadingDeleteCustomer: boolean;

    getCustomers(): void;

    getSelectedCustomer(id: number, force?: boolean): void;

    deleteCustomer(id: number, redirect?: boolean): void;

    clearSelectedCustomer(): void;
}