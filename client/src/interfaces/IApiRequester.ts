import {AccountService, CurrencyService, CustomerService, EmployerService} from "../api/services";

export interface IAPIRequester {
    account: AccountService;
    customer: CustomerService;
    currency: CurrencyService;
    employer: EmployerService;
}