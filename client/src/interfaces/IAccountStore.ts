import {IAccount} from "./IAccount";

export interface IAccountStore {
    accountsMap: Map<IAccount["id"], IAccount>;
    accounts: IAccount[];

    isLoadingGetAccounts: boolean;
    isLoadingDeleteAccount: boolean;

    getAccounts(): void;

    getAccount(id: IAccount["id"]): void;

    deleteAccount(id: IAccount["id"]): void;

    rechargeAccount(number: IAccount["number"], amount: number): Promise<IAccount>;

    withdrawAccount(number: IAccount["number"], amount: number): Promise<IAccount>;

    payAccount(data: {
        from: IAccount,
        to: IAccount,
        amount: IAccount["balance"]
    }): void;
}