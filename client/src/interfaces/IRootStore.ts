import {IAPIRequester} from "./IAPIRequester";
import {IAccountStore} from "./IAccountStore";
import {BrowserHistory} from "history";
import {ICurrencyStore} from "./ICurrencyStore";
import {ICustomerStore} from "./ICustomerStore";
import {IEmployerStore} from "./IEmployerStore";

export interface IRootStore {
    history: BrowserHistory
    requester: IAPIRequester;
    currencyStore: ICurrencyStore;
    accountStore: IAccountStore;
    customerStore: ICustomerStore;
    employerStore: IEmployerStore;

    navigate(url: string, params?: Record<string, any>): void;

}