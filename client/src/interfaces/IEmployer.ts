import {ICustomer} from "./ICustomer";

export interface IEmployer {
    id: number;
    name: string;
    address: string;
    customers: ICustomer[];
}