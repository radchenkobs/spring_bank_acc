import axios, {AxiosInstance} from "axios";
import {IAPIRequester} from "interfaces";
import {Config} from '../config';
import {AccountService, CurrencyService, CustomerService, EmployerService} from "./services";

export class APIRequester implements IAPIRequester {
    protected instance: AxiosInstance;
    //TODO: GraphQl ??
    public account: AccountService;
    public customer: CustomerService;
    public currency: CurrencyService;
    public employer: EmployerService;

    constructor() {
        this.instance = this.createAxiosInstance();
        this.account = new AccountService(this.instance);
        this.customer = new CustomerService(this.instance);
        this.currency = new CurrencyService(this.instance);
        this.employer = new EmployerService(this.instance);
    }

    private createAxiosInstance() {
        const axiosInstance = axios.create({
            withCredentials: false,
            baseURL: Config.API,
        });

        // TODO: axiosInstance.interceptors.request.use()
        // TODO: axiosInstance.interceptors.response.use()

        return axiosInstance;
    }
}