import {IAccount, ICustomer, IEmployer} from "interfaces";

class Api {
    public get accounts(): string {
        return '/accounts'
    }

    public accountsById(id: IAccount["id"]): string {
        return `/accounts/${id}`
    }

    public get accountsRecharge(): string {
        return '/accounts/recharge'
    }

    public get accountsWithdraw(): string {
        return '/accounts/withdraw'
    }

    public get accountsTransfer(): string {
        return '/accounts/transfer'
    }

    public get customers(): string {
        return '/customers'
    }

    public customersById(id: ICustomer["id"]): string {
        return `/customers/${id}`
    }

    public get currencies(): string {
        return '/currencies'
    }

    public get employers(): string {
        return '/employers'
    }

    public employersIdCustomerId(employerId: IEmployer["id"], customerId: ICustomer["id"]): string {
        return `/employers/${employerId}/customer/${customerId}`
    }
}

export default new Api();