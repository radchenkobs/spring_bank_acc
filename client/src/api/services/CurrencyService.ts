import {AxiosInstance} from "axios";
import api from '../api';
import {ICurrency, IResponseData} from "interfaces";

interface IGetCurrenciesReturn extends IResponseData<ICurrency[]> {
}

export class CurrencyService {
    constructor(private readonly instance: AxiosInstance) {
    }

    public async getAll(): Promise<IGetCurrenciesReturn> {
        return this.instance.get(api.currencies)
            .then(({data}) => data);
    }
}