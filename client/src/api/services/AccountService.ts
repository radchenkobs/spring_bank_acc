import {AxiosInstance} from "axios";
import api from '../api';
import {IAccount, ICreateAccountData, ICurrency, ICustomer, IResponseData} from "interfaces";

export class AccountService {
    constructor(private readonly instance: AxiosInstance) {
    }

    public async getAll(): Promise<IResponseData<IAccount[]>> {
        return this.instance.get(api.accounts)
            .then(({data}) => data);
    }

    public async getById(id: IAccount["id"]): Promise<IResponseData<IAccount>> {
        return this.instance.get(api.accountsById(id))
            .then(({data}) => data);
    }

    public async remove(id: IAccount["id"]): Promise<any> {
        return this.instance.delete(api.accountsById(id))
            .then(({data}) => data);
    }

    public async create(customerId: ICustomer["id"], currencyId: ICurrency["id"]): Promise<any> {
        return this.instance.post(api.accounts, {customerId, currencyId} as ICreateAccountData)
            .then(({data}) => data);
    }

    public async recharge(number: IAccount["number"], amount: number): Promise<IResponseData<IAccount>> {
        return this.instance.post(api.accountsRecharge, {number, amount})
            .then(({data}) => data);
    }

    public async withdraw(number: IAccount["number"], amount: number): Promise<IResponseData<IAccount>> {
        return this.instance.post(api.accountsWithdraw, {number, amount})
            .then(({data}) => data);
    }

    public async pay(data: {
        from: IAccount["number"],
        to: IAccount["number"],
        amount: IAccount["balance"]
    }): Promise<IResponseData<unknown>> {
        return this.instance.post(api.accountsTransfer, data)
            .then(({data}) => data);
    }
}