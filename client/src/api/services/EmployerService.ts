import {AxiosInstance} from "axios";
import api from '../api';
import {ICustomer, IEmployer, IResponseData} from "interfaces";

interface IGetEmployersReturn extends IResponseData<IEmployer[]> {
}

interface IEmployerCustomerProps {
    employerId: IEmployer["id"];
    customerId: ICustomer["id"]
}

export class EmployerService {
    constructor(private readonly instance: AxiosInstance) {
    }

    public async getAll(): Promise<IGetEmployersReturn> {
        return this.instance.get(api.employers)
            .then(({data}) => data);
    }

    public async removeCustomer(data: IEmployerCustomerProps): Promise<IGetEmployersReturn> {
        return this.instance.delete(api.employersIdCustomerId(data.employerId, data.customerId))
            .then(({data}) => data);
    }

    public async addCustomer(data: IEmployerCustomerProps): Promise<IGetEmployersReturn> {
        return this.instance.put(api.employersIdCustomerId(data.employerId, data.customerId))
            .then(({data}) => data);
    }
}