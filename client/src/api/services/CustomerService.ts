import {AxiosInstance} from "axios";
import api from '../api';
import {ICustomer, ICustomerCreateData, IResponseData} from "interfaces";

interface IGetCustomersReturn extends IResponseData<ICustomer[]> {
}

interface IGetCustomerReturn extends IResponseData<ICustomer> {
}

export class CustomerService {
    constructor(private readonly instance: AxiosInstance) {
    }

    public async getAll(): Promise<IGetCustomersReturn> {
        return this.instance.get(api.customers)
            .then(({data}) => data);
    }

    public async getById(id: ICustomer["id"]): Promise<IGetCustomerReturn> {
        return this.instance.get(api.customersById(id))
            .then(({data}) => data);
    }

    public async remove(id: ICustomer["id"]): Promise<any> {
        return this.instance.delete(api.customersById(id))
            .then(({data}) => data);
    }

    public async create(data: ICustomerCreateData): Promise<any> {
        return this.instance.post(api.customers, data)
            .then(({data}) => data);
    }

    public async update(id: ICustomer["id"], data: ICustomerCreateData): Promise<any> {
        return this.instance.patch(api.customersById(id), data)
            .then(({data}) => data);
    }
}