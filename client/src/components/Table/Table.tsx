import React, {FC, ReactNode, useCallback, useEffect, useMemo, useRef, useState} from 'react';
import MuiTableContainer from "@mui/material/TableContainer";
import MuiTable from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableFooter from "@mui/material/TableFooter";
import TableRow from "@mui/material/TableRow";
import TablePagination from "@mui/material/TablePagination";
import Paper from "@mui/material/Paper";
import TableHead from "@mui/material/TableHead";
import TableCell from "@mui/material/TableCell";

export interface ITableHeaderOption {
    label: string;
    align?: 'inherit' | 'left' | 'center' | 'right' | 'justify';
}

interface ITableProps {
    headerOptions: ITableHeaderOption[];
    children: ReactNode;
}

export const Table: FC<ITableProps> = (props) => {
    const {headerOptions, children} = props;
    const [maxHeight, setMaxHeight] = useState<number>(0);
    const containerRef = useRef<HTMLDivElement | null>(null);

    const head = useMemo(() => {
        return (
            <TableHead>
                <TableRow>
                    <TableCell/>
                    {headerOptions.map(({label, align}) => (
                        <TableCell
                            key={label}
                            align={align ?? "left"}
                        >
                            {label}
                        </TableCell>
                    ))}
                </TableRow>
            </TableHead>
        )
    }, [headerOptions])

    const calcMaxHeight = useCallback((div: HTMLDivElement | null) => {
        if (!div) return "inherit";

        const max = (div.parentElement?.clientHeight) as number - 24;
        setMaxHeight(max);
    }, []);

    console.log("maxHeight", maxHeight);

    useEffect(() => {
        const element = containerRef.current;
        calcMaxHeight(element)
    }, [containerRef]);

    return (
        <div ref={containerRef}>
            <Paper sx={{width: '100%', overflow: 'hidden', maxHeight: "100%"}}>
                <MuiTableContainer sx={{maxHeight: maxHeight}}>
                    <MuiTable size="small" stickyHeader>
                        {head}
                        <TableBody sx={{flexGrow: 1}}>
                            {children}
                        </TableBody>
                        <TableFooter
                            sx={{
                                left: 0,
                                bottom: 0,
                                zIndex: 2,
                                position: 'sticky',
                                backgroundColor: "white"
                            }}
                        >
                            <TableRow>
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25, {label: 'All', value: -1}]}
                                    colSpan={0}
                                    count={100}
                                    rowsPerPage={10}
                                    page={0}
                                    SelectProps={{
                                        inputProps: {
                                            'aria-label': 'rows per page',
                                        },
                                        native: true,
                                    }}
                                    onPageChange={() => {
                                    }}
                                    onRowsPerPageChange={() => {
                                    }}
                                    // ActionsComponent={TablePaginationActions}
                                />
                            </TableRow>
                        </TableFooter>
                    </MuiTable>
                </MuiTableContainer>
            </Paper>
        </div>
    )
}