import React, {Suspense} from 'react';

export const LazyLoader =
    (Component: React.LazyExoticComponent<any>, showLoader: boolean = true) =>
        (props: any): JSX.Element =>
            (
                <Suspense fallback={showLoader ? <div>Loading...</div> : null}>
                    <Component {...props} />
                </Suspense>
            );
