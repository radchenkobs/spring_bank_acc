const {REACT_APP_API_URL} = process.env;

export class Config {
    public static API: string = REACT_APP_API_URL as string;
}