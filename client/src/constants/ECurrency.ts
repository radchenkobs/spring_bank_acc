//TODO: add id in db. create endpoint. create store
export enum ECurrency {
    USD = "USD",
    EUR = "EUR",
    UAH = "UAH",
    CHF = "CHF",
    GBP = "GBP",
}

export enum ECurrencySymbol {
    USD = "$",
    EUR = "€",
    UAH = "₴",
    CHF = "Fr",
    GBP = "£",
}