import {action, computed, makeObservable, observable, runInAction} from "mobx";
import {BaseStore} from "./BaseStore";
import {IEmployer, IEmployerStore, IError, IRootStore} from "interfaces";

export class EmployerStore extends BaseStore implements IEmployerStore {
    private asyncStatuses = {
        getEmployers: this.createKey('getEmployers'),
    };

    public employersMap: Map<IEmployer["id"], IEmployer> = new Map();
    public employers: IEmployer[] = [];

    constructor(rootStore: IRootStore) {
        super(rootStore);
        makeObservable(this, {
            employersMap: observable,
            employers: observable,

            isLoadingGetEmployers: computed,

            getEmployers: action.bound,
        });
    }

    public get isLoadingGetEmployers() {
        return this.getAsyncStatus(this.asyncStatuses.getEmployers).loading;
    }

    public async getEmployers(): Promise<void> {
        this.setLoading(this.asyncStatuses.getEmployers)
        try {
            const {data, success} = await this.rootStore.requester.employer.getAll();

            if (!success) {
                this.setError(this.asyncStatuses.getEmployers, data.message)
                return;
            }


            runInAction(() => {
                this.employers = [];
                for (const employer of data) {
                    this.employersMap.set(employer.id, employer);
                    this.employers.push(employer);
                }
            });

            this.setSuccess(this.asyncStatuses.getEmployers)

        } catch (err) {
            this.setError(this.asyncStatuses.getEmployers, (err as IError)?.message)
        }
    }
}