import {action, computed, makeObservable, observable, runInAction} from "mobx";
import {BaseStore} from "./BaseStore";
import {ICurrency, ICurrencyStore, IError, IRootStore} from "interfaces";

export class CurrencyStore extends BaseStore implements ICurrencyStore {
    private asyncStatuses = {
        getCurrencies: this.createKey('getCurrencies'),
    };

    public currenciesMap: Map<ICurrency["id"], ICurrency> = new Map();
    private currenciesValuesMap: Map<ICurrency["value"], ICurrency> = new Map();
    public currencies: ICurrency[] = [];

    constructor(rootStore: IRootStore) {
        super(rootStore);
        makeObservable(this, {
            currenciesMap: observable,
            currencies: observable,

            isLoadingCurrencies: computed,

            getCurrencies: action.bound,
            convertFromValueToId: action.bound,
        });
    }

    public get isLoadingCurrencies() {
        return this.getAsyncStatus(this.asyncStatuses.getCurrencies).loading;
    }

    public async getCurrencies(): Promise<void> {
        this.setLoading(this.asyncStatuses.getCurrencies)
        try {
            const {data, success} = await this.rootStore.requester.currency.getAll();

            if (!success) {
                this.setError(this.asyncStatuses.getCurrencies, data.message)
                return;
            }


            runInAction(() => {
                this.currencies = [];
                for (const currency of data) {
                    this.currenciesMap.set(currency.id, currency);
                    this.currenciesValuesMap.set(currency.value, currency);
                    this.currencies.push(currency);
                }
            });

            this.setSuccess(this.asyncStatuses.getCurrencies)

        } catch (err) {
            this.setError(this.asyncStatuses.getCurrencies, (err as IError)?.message)
        }
    }

    public convertFromValueToId(value: string): number {
        const curr = this.currenciesValuesMap.get(value);
        if (!curr) {
            // throw new Error(`No currency from ${value}`)
            return 0;
        }
        return curr.id;
    }
}