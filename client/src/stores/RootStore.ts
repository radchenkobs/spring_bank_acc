import {generatePath} from 'react-router-dom';
import {BrowserHistory, createBrowserHistory} from "history";
import {
    IAccountStore,
    IAPIRequester,
    ICurrencyStore,
    ICustomerEditorStore,
    ICustomerStore,
    IEmployerStore,
    IRootStore
} from "interfaces";
import {APIRequester} from "api";
import {AccountStore} from "./AccountStore";
import {CustomerStore} from "./CustomerStore";
import {CustomerEditorStore} from "./CustomerEditorStore";
import {CurrencyStore} from "./CurrencyStore";
import {EmployerStore} from "./EmployerStore";

const history = createBrowserHistory({
    window,
});

export class RootStore implements IRootStore {
    public history: BrowserHistory = history;

    public requester: IAPIRequester;
    public currencyStore: ICurrencyStore;
    public accountStore: IAccountStore;
    public customerStore: ICustomerStore;
    public customerEditorStore: ICustomerEditorStore;
    public employerStore: IEmployerStore;

    constructor() {
        this.requester = new APIRequester();
        this.currencyStore = new CurrencyStore(this);
        this.accountStore = new AccountStore(this);
        this.customerStore = new CustomerStore(this);
        this.customerEditorStore = new CustomerEditorStore(this);
        this.employerStore = new EmployerStore(this);
    }

    public navigate(url: string, params: Record<string, any> = {}) {
        this.history.push(generatePath(url, params));
    }
}