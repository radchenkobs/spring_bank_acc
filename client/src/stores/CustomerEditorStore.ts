import {BaseStore} from "./BaseStore";
import {
    ICustomer,
    ICustomerCreateData,
    ICustomerEdit,
    ICustomerEditAccount,
    ICustomerEditorStore,
    IError,
    IRootStore
} from "interfaces";
import {action, makeObservable, observable, runInAction} from "mobx";
import {CUSTOMERS_LIST_ROUTE} from "../constants/router";
import {ICustomerEditEmployer} from "../interfaces/ICustomerEdit";

const DEFAULT_CUSTOMER_DATA: ICustomerEdit = {
    name: "",
    email: "",
    age: "",
    accounts: [],
    employers: [],
}

export class CustomerEditorStore extends BaseStore implements ICustomerEditorStore {
    private asyncStatuses = {
        saveProfile: this.createKey("saveProfile"),
    };

    private initCustomer: ICustomer = {} as ICustomer;
    public data: ICustomerEdit = {} as ICustomerEdit;

    constructor(rootStore: IRootStore) {
        super(rootStore);
        makeObservable(this, {
            data: observable,
            initialCustomer: action.bound,
            saveProfile: action.bound,
        });
    }

    private dtoCustomerData(customer: ICustomer): ICustomerEdit {
        return {
            ...customer,
            accounts: customer
                    .accounts?.map(({id, number, balance, currency}) => ({
                        idx: id,
                        number,
                        balance,
                        currency,
                        disabled: true,
                        label: `${currency} - ${number} - ${balance}`
                    } as ICustomerEditAccount))
                || [],
            employers: customer
                    .employers?.map(({id, name}) => ({
                        idx: id,
                        label: name
                    } as ICustomerEditEmployer))
                || []
        }
    }

    public initialCustomer(customer: ICustomer | null) {
        runInAction(() => {
            if (!customer) {
                this.initCustomer = {} as ICustomer;
                this.data = DEFAULT_CUSTOMER_DATA as ICustomerEdit;
                return;
            }
            this.initCustomer = customer;
            this.data = this.dtoCustomerData(customer);
        })
    }

    private async createProfile(data: ICustomerEdit) {
        this.setLoading(this.asyncStatuses.saveProfile)
        try {
            const createData = {
                name: data.name,
                email: data.email,
                age: data.age as number,
                currencies: data.accounts.map(account => account.currency),
                employers: data.employers.map(e => e.idx)
            }
            await this.rootStore.requester.customer.create(createData);

            this.setSuccess(this.asyncStatuses.saveProfile)
            this.rootStore.navigate(CUSTOMERS_LIST_ROUTE)
        } catch (err) {
            this.setError(this.asyncStatuses.saveProfile, (err as IError)?.message)
        }
    }

    private async editProfile(data: ICustomerEdit) {
        this.setLoading(this.asyncStatuses.saveProfile)
        try {
            const updateData = Object.entries(this.initCustomer).reduce((acc, [k, v]) => {
                if (k === "id" || k === "accounts" || k === "employers") return acc;
                // @ts-ignore
                if (data[k] !== v) {
                    // @ts-ignore
                    acc = {...acc, [k]: data[k]};
                }
                return acc;
            }, {} as ICustomerCreateData)

            if (!!Object.values(updateData).length) {
                await this.rootStore.requester.customer.update(data.id as ICustomer["id"], updateData)
            }

            const accToCreate = data.accounts
                .filter(a => !a.idx)
                .map(a => a.currency);

            const accToDelete = this.initCustomer
                .accounts?.filter(({id}) => !data.accounts.some(a => a.idx === id))
                .map(a => a.id) || [];

            if (!!accToCreate.length) {
                await Promise.all(accToCreate.map(c =>
                    this.rootStore.requester.account.create(
                        data.id as ICustomer["id"],
                        this.rootStore.currencyStore.convertFromValueToId(c)
                    )
                ))
            }

            if (!!accToDelete?.length) {
                await Promise.all(accToDelete.map(id => this.rootStore.requester.account.remove(id)))
            }

            const employersToDelete = this.initCustomer
                .employers?.filter(({id}) => !data.employers.some(e => e.idx === id))
                .map(e => e.id) || [];

            const employersToAdd = data.employers
                .filter(({idx}) => !this.initCustomer.employers?.some(e => e.id === idx))
                .map(e => e.idx)

            if (!!employersToDelete.length) {
                await Promise.all(employersToDelete.map(id => this.rootStore.requester.employer.removeCustomer({
                    employerId: id,
                    customerId: data.id as ICustomer["id"]
                })))
            }

            if (!!employersToAdd.length) {
                await Promise.all(employersToAdd.map(id => this.rootStore.requester.employer.addCustomer({
                    employerId: id,
                    customerId: data.id as ICustomer["id"]
                })))
            }

            this.setSuccess(this.asyncStatuses.saveProfile)
            this.rootStore.navigate(CUSTOMERS_LIST_ROUTE)
        } catch (err) {
            this.setError(this.asyncStatuses.saveProfile, (err as IError)?.message)
        }
    }

    public async saveProfile(data: ICustomerEdit): Promise<void> {
        if (!data.id) {
            return this.createProfile(data);
        }
        return this.editProfile(data);
    }
}