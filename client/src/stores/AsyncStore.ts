import {action, makeObservable, observable} from "mobx";
import {IAsyncKey, IAsyncStatus, IAsyncStore} from "interfaces";
import {AsyncStatus} from 'models/AsyncStatus';

export class AsyncStore implements IAsyncStore {
    public _asyncStatusMap: Map<IAsyncKey, IAsyncStatus> = new Map();
    public _asyncErrorsMap: Map<IAsyncKey, string> = new Map();

    constructor() {
        makeObservable(this, {
            _asyncStatusMap: observable,
            _asyncErrorsMap: observable,

            setLoading: action.bound,
            setSuccess: action.bound,
            setError: action.bound,
            createKey: action.bound,
            getAsyncStatus: action.bound,
        });
    }

    public createKey(stringKey: string): IAsyncKey {
        const key = Symbol(stringKey) as unknown as IAsyncKey;
        this._asyncStatusMap.set(key, new AsyncStatus(false, false, false));
        return key;
    }

    public setLoading(key: IAsyncKey): void {
        this._asyncErrorsMap.delete(key);
        this._asyncStatusMap.set(key, new AsyncStatus(true, false, false));
    }

    public setSuccess(key: IAsyncKey): void {
        this._asyncErrorsMap.delete(key);
        this._asyncStatusMap.set(key, new AsyncStatus(false, true, false));
    }

    public setError(key: IAsyncKey, message?: string): void {

        if (message) {
            this._asyncErrorsMap.set(key, message);
        } else {
            this._asyncErrorsMap.delete(key);
        }

        this._asyncStatusMap.set(key, new AsyncStatus(false, false, true));
    }

    public getAsyncStatus(key: IAsyncKey): IAsyncStatus {
        return this._asyncStatusMap.get(key) || new AsyncStatus(false, false, false);
    }

}