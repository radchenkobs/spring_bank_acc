import {action, computed, makeObservable, observable, runInAction} from "mobx";
import {BaseStore} from "./BaseStore";
import {ICustomer, ICustomerStore, IError, IRootStore} from "interfaces";
import {CUSTOMERS_LIST_ROUTE} from "../constants/router";

export class CustomerStore extends BaseStore implements ICustomerStore {
    private asyncStatuses = {
        getCustomers: this.createKey('getCustomers'),
        getSelectedCustomer: this.createKey('getSelectedCustomer'),
        deleteCustomer: this.createKey('deleteCustomer'),
    };

    public customersMap: Map<ICustomer["id"], ICustomer> = new Map();
    public customers: ICustomer[] = [];
    public selectedCustomers: ICustomer | null = null;

    constructor(rootStore: IRootStore) {
        super(rootStore);
        makeObservable(this, {
            customersMap: observable,
            customers: observable,
            selectedCustomers: observable,

            isLoadingGetCustomers: computed,
            isLoadingGetSelectedCustomer: computed,
            isLoadingDeleteCustomer: computed,

            getCustomers: action.bound,
            getSelectedCustomer: action.bound,
            deleteCustomer: action.bound,
            clearSelectedCustomer: action.bound,
        });
    }

    public get isLoadingGetCustomers() {
        return this.getAsyncStatus(this.asyncStatuses.getCustomers).loading;
    }

    public get isLoadingGetSelectedCustomer() {
        return this.getAsyncStatus(this.asyncStatuses.getSelectedCustomer).loading;
    }

    public get isLoadingDeleteCustomer() {
        return this.getAsyncStatus(this.asyncStatuses.deleteCustomer).loading;
    }

    public async getCustomers(): Promise<void> {
        this.setLoading(this.asyncStatuses.getCustomers)
        try {
            const {data, success} = await this.rootStore.requester.customer.getAll();

            if (!success) {
                this.setError(this.asyncStatuses.getCustomers, data.message)
                return;
            }


            runInAction(() => {
                this.customers = [];
                for (const customer of data) {
                    this.customersMap.set(customer.id, customer);
                    this.customers.push(customer);
                }
                this.setSuccess(this.asyncStatuses.getCustomers)
            });

        } catch (err) {
            this.setError(this.asyncStatuses.getCustomers, (err as IError)?.message)
        }
    }

    public async getSelectedCustomer(id: number, force = false): Promise<void> {
        const customer = this.customersMap.get(id);
        if (customer && !force) {
            this.selectedCustomers = customer;
            return;
        }

        this.setLoading(this.asyncStatuses.getSelectedCustomer)
        try {
            const {data, success} = await this.rootStore.requester.customer.getById(id);

            if (!success) {
                this.setError(this.asyncStatuses.getSelectedCustomer, data.message)
                return;
            }


            runInAction(() => {
                this.customersMap.set(data.id, data)
                this.selectedCustomers = data;
                this.setSuccess(this.asyncStatuses.getSelectedCustomer)
            });

        } catch (err) {
            this.setError(this.asyncStatuses.getSelectedCustomer, (err as IError)?.message)
        }
    }

    public async deleteCustomer(id: number, redirect: boolean = false): Promise<void> {
        try {
            this.setLoading(this.asyncStatuses.deleteCustomer)
            await this.rootStore.requester.customer.remove(id);

            runInAction(() => {
                this.customersMap.delete(id);
                this.customers = this.customers.filter((customer) => customer.id !== id);
                this.setSuccess(this.asyncStatuses.deleteCustomer)
            })
            if (redirect) {
                this.rootStore.navigate(CUSTOMERS_LIST_ROUTE)
            }
        } catch (err) {
            this.setError(this.asyncStatuses.deleteCustomer, (err as IError)?.message)
        }
    }

    public clearSelectedCustomer() {
        this.selectedCustomers = null;
    }

}