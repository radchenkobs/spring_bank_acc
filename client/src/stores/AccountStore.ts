import {action, computed, makeObservable, observable, runInAction} from "mobx";
import {BaseStore} from "./BaseStore";
import {IAccount, IAccountStore, IError, IRootStore} from "interfaces";

export class AccountStore extends BaseStore implements IAccountStore {
    private asyncStatuses = {
        getAccount: this.createKey('getAccount'),
        getAccounts: this.createKey('getAccounts'),
        deleteAccount: this.createKey('deleteAccount'),
        rechargeAccount: this.createKey('rechargeAccount'),
        withdrawAccount: this.createKey('withdrawAccount'),
        payAccount: this.createKey('payAccount'),
    };

    public accountsMap: Map<IAccount["id"], IAccount> = new Map();
    public accounts: IAccount[] = [];

    constructor(rootStore: IRootStore) {
        super(rootStore);
        makeObservable(this, {
            accountsMap: observable,
            accounts: observable,

            isLoadingGetAccounts: computed,
            isLoadingDeleteAccount: computed,

            getAccounts: action.bound,
            getAccount: action.bound,
            deleteAccount: action.bound,
            rechargeAccount: action.bound,
            withdrawAccount: action.bound,
            payAccount: action.bound,
        });
    }

    public get isLoadingGetAccounts() {
        return this.getAsyncStatus(this.asyncStatuses.getAccounts).loading;
    }

    public get isLoadingDeleteAccount() {
        return this.getAsyncStatus(this.asyncStatuses.deleteAccount).loading;
    }

    public async getAccounts(): Promise<void> {
        this.setLoading(this.asyncStatuses.getAccounts)
        try {
            const {data, success} = await this.rootStore.requester.account.getAll();

            if (!success) {
                this.setError(this.asyncStatuses.getAccounts, data.message)
                return;
            }


            runInAction(() => {
                this.accounts = [];
                for (const account of data) {
                    this.accountsMap.set(account.id, account);
                    this.accounts.push(account);
                }
            });

            this.setSuccess(this.asyncStatuses.getAccounts)

        } catch (err) {
            this.setError(this.asyncStatuses.getAccounts, (err as IError)?.message)
        }
    }

    public async getAccount(id: IAccount["id"]): Promise<void> {
        this.setLoading(this.asyncStatuses.getAccount)
        try {
            const {data, success} = await this.rootStore.requester.account.getById(id);

            if (!success) {
                this.setError(this.asyncStatuses.getAccount, data.message)
                return;
            }

            runInAction(() => {
                this.accountsMap.set(data.id, data);
                if (this.accounts.find(a => a.id === data.id)) {
                    this.accounts = this.accounts.map(a => {
                        if (a.id === data.id) {
                            return data;
                        }
                        return a;
                    })
                } else {
                    this.accounts.push(data);
                }
            });

            this.setSuccess(this.asyncStatuses.getAccount)

        } catch (err) {
            this.setError(this.asyncStatuses.getAccount, (err as IError)?.message)
        }
    }

    public async deleteAccount(id: number): Promise<void> {
        try {
            this.setLoading(this.asyncStatuses.deleteAccount)
            await this.rootStore.requester.account.remove(id);

            runInAction(() => {
                this.accountsMap.delete(id);
                this.accounts = this.accounts.filter((account) => account.id !== id);
            })

            this.setSuccess(this.asyncStatuses.deleteAccount)
        } catch (err) {
            this.setError(this.asyncStatuses.deleteAccount, (err as IError)?.message)
        }
    }

    public async rechargeAccount(number: IAccount["number"], amount: number): Promise<IAccount> {
        try {
            this.setLoading(this.asyncStatuses.rechargeAccount)
            const {data} = await this.rootStore.requester.account.recharge(number, amount);

            runInAction(() => {
                this.accountsMap.set(data.id, data);
                this.accounts = this.accounts.map((account) => data.id === account.id ? data : account);
            })

            this.setSuccess(this.asyncStatuses.rechargeAccount)
            return data;
        } catch (err) {
            this.setError(this.asyncStatuses.rechargeAccount, (err as IError)?.message)
            return {} as IAccount;
        }
    }

    public async withdrawAccount(number: IAccount["number"], amount: number): Promise<IAccount> {
        try {
            this.setLoading(this.asyncStatuses.withdrawAccount)
            const {data} = await this.rootStore.requester.account.withdraw(number, amount);

            runInAction(() => {
                this.accountsMap.set(data.id, data);
                this.accounts = this.accounts.map((account) => data.id === account.id ? data : account);
            })

            this.setSuccess(this.asyncStatuses.withdrawAccount)
            return data;
        } catch (err) {
            this.setError(this.asyncStatuses.withdrawAccount, (err as IError)?.message)
            return {} as IAccount;
        }
    }

    public async payAccount(
        data: {
            from: IAccount,
            to: IAccount,
            amount: IAccount["balance"]
        }): Promise<void> {
        try {
            this.setLoading(this.asyncStatuses.payAccount)
            await this.rootStore.requester.account.pay({
                ...data,
                from: data.from.number,
                to: data.to.number,
            });

            await runInAction(async () => {
                await this.getAccount(data.from.id);
                await this.getAccount(data.to.id);
            })

            this.setSuccess(this.asyncStatuses.payAccount)
        } catch (err) {
            this.setError(this.asyncStatuses.payAccount, (err as IError)?.message)
        }
    }

}