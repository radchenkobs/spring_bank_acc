export const formattedCardNumber = (number: string): string => {
    if (!number || !number.length) return number;
    return number
        .split(/(\d{4})/gm)
        .filter(s => !!s)
        .join(" ")
}