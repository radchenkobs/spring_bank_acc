# homeworks

- #### [main](https://gitlab.com/radchenkobs/spring_bank_acc)
- #### [homework1](https://gitlab.com/radchenkobs/spring_bank_acc/-/tree/hw1)
- #### [homework2](https://gitlab.com/radchenkobs/spring_bank_acc/-/tree/hw2)
- #### [homework3](https://gitlab.com/radchenkobs/spring_bank_acc/-/tree/hw3)

### PROD: start (from java)

- ```mvn clean install```

- ```java -jar -Dspring.profiles.active=prod target/bank.jar```

### DEV: start

- ```(start spring)```

- ```npm run dev:client```

#### PORTS

- prod: 8080
- dev:
    - back: 9000
    - front: 3000

#### REST

- customer
    - GET /api/customers - get all customer
    - GET /api/customers/:id - get customer by id
    - POST /api/customers - create customer
    - DELETE /api/customers/:id - delete customer
    - PATCH /api/customers/:id - top up account

- accounts
    - GET /api/accounts - get all accounts
    - GET /api/accounts/:id - get account by id
    - POST /api/accounts - create account
    - DELETE /api/accounts/:id - delete account
    - POST /api/accounts/recharge - top up account
    - POST /api/accounts/withdraw - withdraw money from an account
    - POST /api/accounts/withdraw - withdraw money from an account
    - POST /api/accounts/transfer - transfer funds

- employers
